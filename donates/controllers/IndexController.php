<?php

namespace app\modules\donates\controllers;

class IndexController extends \app\modules\donates\components\BaseController
{
    public function actionIndex()
    {
        return $this->render('index', $this->prepareViewParams());
    }
}
