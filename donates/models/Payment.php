<?php

namespace app\modules\donates\models;

use Yii;
use faryshta\validators\EnumValidator;

/**
 * This is the model class for table "{{%payments}}".
 *
 * @property int $id
 * @property int|null $donateId
 * @property int $amount
 * @property string $type
 * @property string $status
 * @property string|null $error
 * @property string $createdAt
 * @property string|null $updatedAt
 * @property boolean $notificationSent
 * 
 * @property string $statusDesc Description of the 'status'
 * @property string $typeDesc Description of the 'type'
 *
 * @property bool $isSucceeded
 * @property bool $isPending
 * @property bool $isCanceled
 * @property bool $isSingle
 * @property bool $isMonthly
 * @property bool $isMonthlyFirst
 * @property bool $isMonthlyNext
 * 
 * @property string $paymentServiceOperationId
 * @property string|null $recurrentPaymentOperationId
 * 
 * @property Donate $donate
 * @property PaymentServiceOperation $paymentServiceOperation
 */
class Payment extends \yii\db\ActiveRecord
{
    use \faryshta\base\EnumTrait;

    const TYPE_SINGLE = 'single';
    const TYPE_MONTHLY_FIRST = 'monthly-first';
    const TYPE_MONTHLY_NEXT = 'monthly-next';

    const STATUS_PENDING = 'pending';
    const STATUS_SUCCEEDED = 'succeeded';
    const STATUS_CANCELED = 'canceled';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%payments}}';
    }


    public function enums()
    {
        return [
            'status' => [
                self::STATUS_PENDING => 'обработка',
                self::STATUS_SUCCEEDED => 'успешно завершено',
                self::STATUS_CANCELED => 'отменено',
            ],
            'type' => [
                self::TYPE_SINGLE => 'одиночный',
                self::TYPE_MONTHLY_FIRST => 'первый ежемесячный',
                self::TYPE_MONTHLY_NEXT => 'очередной ежемесячный',
            ],
        ];
    }

    public function getStatusDesc()
    {
        return $this->getAttributeDesc('status');
    }

    public function getTypeDesc()
    {
        return $this->getAttributeDesc('type');
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['createdAt', 'default', 'value' => date('Y-m-d H:i:s')],
            ['updatedAt', 'default', 'value' => date('Y-m-d H:i:s')],

            [['createdAt', 'status', 'type', 'donateId'], 'required'],

            ['type', 'string'],
            ['type', EnumValidator::class],

            ['amount', 'integer', 'min' => Donate::AMOUNT_MIN_VALUE, 'max' => Donate::AMOUNT_MAX_VALUE],

            ['status', 'string'],
            ['status', EnumValidator::class],

            [['createdAt', 'updatedAt'], 'safe'],
            [['error'], 'string', 'max' => 255],
            ['notificationSent', 'boolean'],

            ['donateId', 'integer'],
            ['donateId', 'exist', 'skipOnError' => true, 'targetClass' => Donate::class, 'targetAttribute' => ['donateId' => 'id']],
        ];
    }


    public function beforeValidate()
    {
        if (!$this->isNewRecord) {
            $this->updatedAt = null;
        }
        return parent::beforeValidate();
    }


    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'donateId' => 'ID Доната',
            'type' => 'Тип',
            'amount' => 'Сумма',
            'status' => 'Статус',
            'error' => 'Сообщение об ошибке',
            'createdAt' => 'Создано',
            'updatedAt' => 'Изменено',
            'notificationSent' => 'Уведомление донору отправлено',
        ];
    }

    /**
     * Gets query for [[Donate]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDonate()
    {
        return $this->hasOne(Donate::class, ['id' => 'donateId']);
    }

    /**
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentServiceOperation()
    {
        return $this->hasOne(PaymentServiceOperation::class, ['paymentId' => 'id']);
    }


    public function getPaymentServiceOperationId(): string
    {
        $operation = $this->paymentServiceOperation;
        if (!$operation) {
            Yii::warning('Operation not found');
            return '';
        }
        return $operation->operationId;
    }

    public function getRecurrentPaymentOperationId(): string
    {
        $operation = PaymentServiceOperation::find()
            ->where(['donateId' => $this->donateId])
            ->andWhere('`recurrentOperationId` IS NOT NULL')
            ->orderBy(['paymentId' => SORT_DESC])
            ->limit(1)
            ->one();
        /** @var PaymentServiceOperation $operation */
        if (!$operation) {
            Yii::warning('Operation not found');
            $resultId = '';
        } else {
            $resultId = $operation->recurrentOperationId;
        }
        if (empty($resultId) && $this->isMonthlyNext) {
            Yii::warning('Using operationId as recurrentOperationId');
            $resultId = $operation->operationId; //FIXME only for YandexKAssa
        }
        return $resultId;
    }

    public function getIsSucceeded(): bool
    {
        return $this->status == self::STATUS_SUCCEEDED;
    }

    public function getIsPending(): bool
    {
        return $this->status == self::STATUS_PENDING;
    }

    public function getIsCanceled(): bool
    {
        return $this->status == self::STATUS_CANCELED;
    }

    public function getIsSingle(): bool
    {
        return $this->type == self::TYPE_SINGLE;
    }

    public function getIsMonthly(): bool
    {
        return $this->type == self::TYPE_MONTHLY_FIRST || $this->type == self::TYPE_MONTHLY_NEXT;
    }

    public function getIsMonthlyNext(): bool
    {
        return $this->type == self::TYPE_MONTHLY_NEXT;
    }

    public function getIsMonthlyFirst(): bool
    {
        return $this->type == self::TYPE_MONTHLY_FIRST;
    }

    public function markAsNotificationSentAndSave(): bool
    {
        if ($this->notificationSent) return false;
        $this->notificationSent = true;
        return $this->save();
    }


    /**
     * 
     * @param array $types Array of Payment::TYPE_*
     * @param int $limit 
     * @return Payment[]
     */
    public static function findLastSucceeded($types = [self::TYPE_SINGLE, self::TYPE_MONTHLY_FIRST], $limit = 10)
    {
        return self::find()
            ->with('donate')
            ->where([
                'status' => Payment::STATUS_SUCCEEDED,
                'type' => $types
            ])
            ->orderBy(['id' => SORT_DESC])
            ->limit($limit)
            ->all();
    }
}
