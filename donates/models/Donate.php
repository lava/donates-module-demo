<?php

namespace app\modules\donates\models;

use DateInterval;
use DateTime;
use Exception;
use Yii;

/**
 * This is the model class for table "{{%donates}}".
 *
 * @property int $id
 * @property string $hash
 * @property int $donorId
 * @property string $createdAt
 * @property string|null $updatedAt
 * @property int $amount
 * @property bool $anonymous
 * @property bool $monthly
 * @property bool $enabled
 * @property string|null $lastPaidAt
 * @property string|null $nextPaymentAt
 * 
 * @property-read string|null $recurrentPaymentOperationId
 * 
 * @property-read Payment|null $firstPayment
 * @property-read Payment|null $lastPayment
 * @property-read Payment|null $lastSuccefulPayment
 * @property-read bool $paid True if last payment was successful
 * 
 * @property-read bool $daysTillNextPayment
 * @property-read bool $isReadyForRepeat True if donate is monthly, enabled and 'nextPaymentAt' is today or in the past
 *
 * @property-read DonateCategory $category
 * @property-read Donor $donor
 * @property-read Payment[] $payments
 * @property-read PaymentServiceOperation[] $paymentServiceOperations
 */
class Donate extends \yii\db\ActiveRecord
{
    const HASH_MAX_LENGTH = 64;
    const AMOUNT_MIN_VALUE = 20;
    const AMOUNT_MAX_VALUE = 250000;

    /**
     * Undocumented variable
     *
     * @deprecated 
     * @var integer
     */
    protected $daysTillNextPayment = null;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%donates}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['createdAt', 'default', 'value' => date('Y-m-d H:i:s')],
            ['updatedAt', 'default', 'value' => date('Y-m-d H:i:s')],
            ['hash', 'default', 'value' => $this->generateHash()],
            ['nextPaymentAt', 'default', 'value' => function ($model) {
                if ($model->monthly) {
                    return $this->calculateNextPaymentDateForNextMonth($model->createdAt)->format('Y-m-d H:i:s');
                }
                return null;
            }],

            [['hash', 'donorId', 'categoryId', 'createdAt', 'amount'], 'required'],

            ['anonymous', 'default', 'value' => false],
            ['anonymous', 'boolean'],

            ['monthly', 'default', 'value' => false],
            ['monthly', 'boolean'],

            ['enabled', 'default', 'value' => true],
            ['enabled', 'boolean'],

            ['amount', 'integer', 'min' => self::AMOUNT_MIN_VALUE, 'max' => self::AMOUNT_MAX_VALUE],

            [['createdAt', 'updatedAt', 'lastPaidAt', 'nextPaymentAt'], 'safe'],

            ['hash', 'string', 'max' => 64],

            ['categoryId', 'integer'],
            ['categoryId', 'exist', 'skipOnError' => true, 'targetClass' => DonateCategory::class, 'targetAttribute' => ['categoryId' => 'id']],

            ['donorId', 'integer'],
            ['donorId', 'exist', 'skipOnError' => true, 'targetClass' => Donor::class, 'targetAttribute' => ['donorId' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'hash' => 'Хэш',
            'donorId' => 'ID Донора',
            'createdAt' => 'Создано',
            'updatedAt' => 'Обновлено',
            'amount' => 'Сумма',
            'anonymous' => 'Анонимно',
            'monthly' => 'Ежемесячно',
            'enabled' => 'Активно',
            'nextPaymentAt' => 'Следующий платёж',
        ];
    }

    public function beforeValidate()
    {
        if (!$this->isNewRecord) {
            $this->updatedAt = null;
        }
        return parent::beforeValidate();
    }

    /**
     * Gets query for [[Category]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(DonateCategory::class, ['id' => 'categoryId']);
    }

    /**
     * Gets query for [[Donor]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDonor()
    {
        return $this->hasOne(Donor::class, ['id' => 'donorId']);
    }

    /**
     * Gets query for [[Payments]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPayments()
    {
        return $this->hasMany(Payment::class, ['donateId' => 'id']);
    }

    /**
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentServiceOperations()
    {
        return $this->hasMany(PaymentServiceOperation::class, ['donateId' => 'id']);
    }


    /**
     * Returns number of days relative to today. 
     * It's negative if nextPaymentAt is in the past.
     *  
     * @return int
     * @throws Exception
     */
    public function getDaysTillNextPayment(): int
    {
        $now = new DateTime();
        $next = new DateTime($this->nextPaymentAt);
        $interval = $next->diff($now);
        if ($interval === false) {
            throw new Exception("Invalid date value");
        }
        $days = $interval->invert == 1 ? -$interval->days : $interval->days;
        return $days;
    }



    public function getFirstPayment(): ?Payment
    {
        return Payment::find()
            ->where(['donateId' => $this->id])
            ->orderBy(['id' => SORT_ASC])
            ->limit(1)
            ->one();
    }

    public function getLastPayment(): ?Payment
    {
        return Payment::find()
            ->where(['donateId' => $this->id])
            ->orderBy(['id' => SORT_DESC])
            ->limit(1)
            ->one();
    }

    public function getLastSuccefulPayment(): ?Payment
    {
        return Payment::find()
            ->where(['donateId' => $this->id, 'status' => Payment::STATUS_SUCCEEDED])
            ->orderBy(['id' => SORT_DESC])
            ->limit(1)
            ->one();
    }


    public function getRecurrentPaymentOperationId(): string
    {
        $operation = $this->getPaymentServiceOperations()
            ->where('`recurrentOperationId` IS NOT NULL')
            ->orderBy(['paymentId' => SORT_DESC])
            ->limit(1)
            ->one();
        /** @var PaymentServiceOperation $operation */
        if (!$operation) {
            Yii::warning('Operation not found');
            return '';
        }
        return $operation->recurrentOperationId;
    }

    public function getPaid(): bool
    {
        $payment = $this->getLastPayment();
        if ($payment && $payment->isSucceeded) return true;
        return false;
    }

    /**
     * Checks nextPaymentAt date and returns true if it is today or in the past
     *
     * @return boolean
     */
    public function getIsReadyForRepeat(): bool
    {
        if (!$this->monthly || !$this->enabled) return false;
        return $this->daysTillNextPayment >= 0;
    }



    protected function generateHash(): string
    {
        return md5(uniqid()) . md5($this->createdAt . $this->donorId . $this->amount);
    }

    /**
     * Calculates and sets NextPaymentAt value
     *
     * @param string|\DateTime $baseDate If empty - current date will be used.
     * @return DateTime
     */
    public function calculateNextPaymentDateForCurrentMonth($baseDate = ''): DateTime
    {
        if (empty($baseDate)) {
            $baseDate = new DateTime();
        } else if (is_string($baseDate)) {
            $baseDate = new DateTime($baseDate);
        }

        $now = new DateTime();
        $time = $baseDate->format('H:i:s');
        $day = (int)$baseDate->format('d');
        $year = $now->format('Y');
        $month = $now->format('m');
        $nextDate = $this->createValidDateTime(sprintf('%s-%s-%02d %s', $year, $month, $day, $time));
        while (!$nextDate) {
            $day--;
            if ($day < 1) {
                Yii::warning('Failed to calculate nextPaymentAt');
                $nextDate = $baseDate->add(new \DateInterval('P1M'));
                break;
            }
            $nextDate = $this->createValidDateTime(sprintf('%s-%s-%02d %s', $year, $month, $day, $time));
        }
        $this->nextPaymentAt = $nextDate->format('Y-m-d H:i:s');
        return $nextDate;
    }


    /**
     * Calculates and sets NextPaymentAt value
     *
     * @param string $baseDate
     * @return DateTime
     */
    public function calculateNextPaymentDateForNextMonth($baseDate = ''): DateTime
    {
        if (empty($baseDate)) {
            $baseDate = new DateTime();
        } else if (is_string($baseDate)) {
            $baseDate = new DateTime($baseDate);
        }

        $now = new DateTime();
        $time = $baseDate->format('H:i:s');
        $day = (int)$baseDate->format('d');
        $year = $now->format('Y');
        $month = (int)$now->format('m');
        $month++;
        if ($month > 12) $month = 1;
        $nextDate = $this->createValidDateTime(sprintf('%s-%02d-%02d %s', $year, $month, $day, $time));
        while (!$nextDate) {
            $day--;
            if ($day < 1) {
                Yii::warning('Failed to calculate nextPaymentAt');
                $nextDate = $baseDate->add(new \DateInterval('P1M'));
                break;
            }
            $nextDate = $this->createValidDateTime(sprintf('%s-%02d-%02d %s', $year, $month, $day, $time));
        }
        $this->nextPaymentAt = $nextDate->format('Y-m-d H:i:s');
        return $nextDate;
    }

    private function createValidDateTime(string $formatedValue): ?DateTime
    {
        $datetime = new DateTime($formatedValue);
        if (!$datetime) return null;
        if ($datetime->format('Y-m-d') != mb_substr($formatedValue, 0, 10)) return null;
        return $datetime;
    }


    /**
     * Adds days to NextPaymentAt and sets this value
     *
     * @param integer $days
     * @return string New value
     */
    public function addDaysToNextPaymentDate(int $days = 1): string
    {
        $date = new DateTime($this->nextPaymentAt);
        $this->nextPaymentAt = $date->add(new DateInterval('P' . $days . 'D'))->format('Y-m-d H:i:s');
        return $this->nextPaymentAt;
    }


    /**
     *
     * @return \yii\db\ActiveQuery
     */
    public static function queryAllEnabled()
    {
        return self::find()->where(['enabled' => true]);
    }

    /**
     *
     * @return \yii\db\ActiveQuery
     */
    public static function queryAllMonthlyEnabled()
    {
        return self::find()->where(['enabled' => true, 'monthly' => true]);
    }

    /**
     *
     * @param string $hash
     * @return Donate|null
     */
    public static function findByHash(string $hash)
    {
        return self::findOne(['hash' => $hash]);
    }
}
