<?php

namespace app\modules\donates\models;

use Yii;

/**
 * This is the model class for table "{{%payment_service_operations}}".
 *
 * @property int $donateId
 * @property int $paymentId
 * @property string $operationId
 * @property string|null $recurrentOperationId
 * @property string|null $request
 * @property string|null $response
 *
 * @property Donate $donate
 * @property Payment $payment
 */
class PaymentServiceOperation extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%payment_service_operations}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['donateId', 'paymentId', 'operationId'], 'required'],
            [['donateId', 'paymentId'], 'integer'],
            [['operationId', 'recurrentOperationId'], 'string', 'max' => 255],
            [['paymentId'], 'unique'],
            [['response', 'request'], 'string'],
            [['donateId'], 'exist', 'skipOnError' => true, 'targetClass' => Donate::class, 'targetAttribute' => ['donateId' => 'id']],
            [['paymentId'], 'exist', 'skipOnError' => true, 'targetClass' => Payment::class, 'targetAttribute' => ['paymentId' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'donateId' => 'ID Доната',
            'paymentId' => 'ID Платежа',
            'operationId' => 'ID платёжной операции',
            'recurrentOperationId' => 'ID рекуррентной платёжной операции',
            'request' => 'Запрос',
            'response' => 'Ответ',
        ];
    }

    /**
     * Gets query for [[Donate]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDonate()
    {
        return $this->hasOne(Donate::class, ['id' => 'donateId']);
    }

    /**
     * Gets query for [[Payment]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPayment()
    {
        return $this->hasOne(Payment::class, ['id' => 'paymentId']);
    }
}
