<?php

namespace app\modules\donates\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\donates\models\Donate;

/**
 * DonateSearch represents the model behind the search form of `app\modules\donates\models\Donate`.
 */
class DonateSearch extends Donate
{
    public $donorData;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'amount'], 'integer'],
            // ['donorId', 'integer'],
            ['donorData', 'string'],
            [['monthly', 'enabled'], 'boolean'],
            [['hash', 'createdAt', 'nextPaymentAt'], 'safe'],
        ];
    }

    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'donorData' => 'Донор',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Donate::find()->joinWith('donor d');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC
                ]
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'donates.id' => $this->id,
            // 'donorId' => $this->donorId,
            'donates.createdAt' => $this->createdAt,
            'amount' => $this->amount,
            'monthly' => $this->monthly,
            'donates.enabled' => $this->enabled,
            'nextPaymentAt' => $this->nextPaymentAt,
        ]);

        if (is_numeric($this->donorData)) {
            if (mb_strlen($this->donorData) < 7) { //id
                $query->andFilterWhere(['donorId' => $this->donorData]);
            } else { //phone
                $query->andFilterWhere(['like', 'd.phone', $this->donorData]);
            }
        } else if (mb_stripos($this->donorData, '@') !== false) { //email
            $query->andFilterWhere(['like', 'd.email', $this->donorData]);
        } else { //fio
            $query->andFilterWhere(['like', 'd.name', $this->donorData]);
        }

        $query->andFilterWhere(['like', 'hash', $this->hash]);

        return $dataProvider;
    }
}
