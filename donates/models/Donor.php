<?php

namespace app\modules\donates\models;

use Yii;

/**
 * This is the model class for table "{{%donors}}".
 *
 * @property int $id
 * @property string|null $name
 * @property string|null $phone
 * @property string $email
 * 
 * @property int $totalDonatesCount
 * @property int $totalPaymentsAmount
 *
 * @property Donate[] $donates
 */
class Donor extends \yii\db\ActiveRecord
{
    protected $totalDonatesCount = null;
    protected $totalPaymentsAmount = null;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%donors}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'unique'],
            [['name', 'phone', 'email'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'ФИО',
            'phone' => 'Телефон',
            'email' => 'Почта',
        ];
    }

    /**
     * Gets query for [[Donates]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDonates()
    {
        return $this->hasMany(Donate::class, ['donorId' => 'id']);
    }


    public function getTotalDonatesCount(): int
    {
        if ($this->totalDonatesCount === null) {
            $this->totalDonatesCount = Donate::find()
                ->where(['donorId' => $this->id])
                ->count();
        }
        return $this->totalDonatesCount;
    }

    public function setTotalDonatesCount($value)
    {
        $this->totalDonatesCount = (int)$value;
    }

    public function getTotalPaymentsAmount(): int
    {
        if ($this->totalPaymentsAmount === null) {
            $this->totalPaymentsAmount = (int) Donate::find()
                ->alias('d')
                ->joinWith('payments p')
                ->select(['SUM(p.amount)'])
                ->where(['d.donorId' => $this->id, 'p.status' => Payment::STATUS_SUCCEEDED])
                ->scalar();
        }
        return $this->totalPaymentsAmount;
    }

    public function setTotalPaymentsAmount($value)
    {
        $this->totalPaymentsAmount = (int)$value;
    }
}
