<?php

namespace app\modules\donates\models;

use Yii;

/**
 * This is the model class for table "{{%donate_categories}}".
 *
 * @property int $id
 * @property string $name
 *
 * @property Donate[] $donates
 */
class DonateCategory extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%donate_categories}}';
    }


    public static function getDefaultCategoryId(): int
    {
        return 1;
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }

    /**
     * Gets query for [[Donates]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDonates()
    {
        return $this->hasMany(Donate::class, ['categoryId' => 'id']);
    }
}
