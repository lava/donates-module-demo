<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\donates\models\YandexKassaOperationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Yandex Kassa Operations';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="yandex-kassa-operation-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Yandex Kassa Operation', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'donatePaymentId',
            'createdAt',
            'paymentId',
            'type',
            //'response',
            //'status',
            //'amount',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
