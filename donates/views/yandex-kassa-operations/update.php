<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\donates\models\YandexKassaOperation */

$this->title = 'Update Yandex Kassa Operation: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Yandex Kassa Operations', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="yandex-kassa-operation-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
