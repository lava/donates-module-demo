<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\donates\models\YandexKassaOperation */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="yandex-kassa-operation-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'donatePaymentId')->textInput() ?>

    <?= $form->field($model, 'createdAt')->textInput() ?>

    <?= $form->field($model, 'paymentId')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'type')->dropDownList([ 'create' => 'Create', 'update_status' => 'Update status', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'response')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'status')->dropDownList([ 'succeeded' => 'Succeeded', 'pending' => 'Pending', 'canceled' => 'Canceled', 'waiting_for_capture' => 'Waiting for capture', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'amount')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
