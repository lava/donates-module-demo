<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\donates\models\YandexKassaOperationSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="yandex-kassa-operation-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'donatePaymentId') ?>

    <?= $form->field($model, 'createdAt') ?>

    <?= $form->field($model, 'paymentId') ?>

    <?= $form->field($model, 'type') ?>

    <?php // echo $form->field($model, 'response') ?>

    <?php // echo $form->field($model, 'status') ?>

    <?php // echo $form->field($model, 'amount') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
