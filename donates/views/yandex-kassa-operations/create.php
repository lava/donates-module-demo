<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\donates\models\YandexKassaOperation */

$this->title = 'Create Yandex Kassa Operation';
$this->params['breadcrumbs'][] = ['label' => 'Yandex Kassa Operations', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="yandex-kassa-operation-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
