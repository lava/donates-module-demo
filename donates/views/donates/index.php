<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\donates\models\DonateSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Donates';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="donate-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Donate', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); 
    ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'hash',
            'donorId',
            'createdAt',
            'updatedAt',
            //'amount',
            //'anonymous',
            //'monthly',
            //'enabled',
            //'nextPaymentAt',
            //'ykPaymentId',
            //'ykSavedPaymentId',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>