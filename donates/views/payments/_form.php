<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\donates\models\Payment */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="payment-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'donateId')->textInput() ?>

    <?= $form->field($model, 'type')->dropDownList([ 'single' => 'Single', 'monthly-first' => 'Monthly-first', 'monthly-next' => 'Monthly-next', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'status')->dropDownList([ 'succeeded' => 'Succeeded', 'pending' => 'Pending', 'canceled' => 'Canceled', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'error')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'createdAt')->textInput() ?>

    <?= $form->field($model, 'updatedAt')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
