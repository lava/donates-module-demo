<?php

/** @var \yii\web\View $this */
/** @var string $content */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap4\Nav;
use yii\bootstrap4\NavBar;
use yii\bootstrap4\Breadcrumbs;
use app\widgets\Alert;

\app\modules\donates\DonatesAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">

<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?> - Донаты - <?= Yii::$app->name ?></title>
    <?php $this->head() ?>
</head>

<body>
    <?php $this->beginBody() ?>

    <div class="container">
        <?php
        NavBar::begin([
            'brandLabel' => 'Донаты',
            'brandUrl' => Url::to('/donates'),
            'options' => [
                'class' => 'navbar navbar-light navbar-expand-lg',
            ],
        ]);

        $items = [
            '<div class="navbar-text"><a href="/" title="Вернуться на сайт"><i class="fas fa-home"></i></a></div>',
        ];


        echo Nav::widget([
            'options' => ['class' => 'nav justify-content-end'],
            'items' => $items,
        ]);
        NavBar::end();
        ?>

        <div class="container">
            <?= Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                'homeLink' => ['label' => 'CP', 'url' => '/cp'],
            ]) ?>
            <?= Alert::widget() ?>
            <?= $content ?>
        </div>
    </div>

    <footer class="footer">
        <div class="container">
            <p class="float-left">
                &copy; <a href="https://centrra.ru/" title="Автономная некоммерческая организация «Центр социальной реабилитации и адаптации»">
                    АНО «ЦРА»</a> <?= date('Y') ?>
            </p>
            <p class="float-right"></p>
        </div>
    </footer>

    <?php $this->endBody() ?>
</body>

</html>
<?php $this->endPage() ?>