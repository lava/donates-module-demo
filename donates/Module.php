<?php

namespace app\modules\donates;

use Yii;

/**
 * api module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\donates\controllers';


    /**
     * @var string The prefix for user module URL.
     *
     * @See [[GroupUrlRule::prefix]]
     */
    public $urlPrefix = 'donates';

    public $rbacPermissionForAccess = '';
    public $rbacPermissionForRead = '';
    public $rbacPermissionForWrite = '';
}
