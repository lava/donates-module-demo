<?php

namespace app\modules\donates\commands;

use Yii;

use yii\console\widgets\Table;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\VarDumper;
use yii\helpers\Console;

use app\modules\donates\components\DbSessionSettings;
use app\modules\donates\components\DonateService;

use app\modules\donates\models\Donate;
use app\modules\donates\models\Donor;
use app\modules\donates\models\Payment;
use app\modules\donates\models\PaymentServiceOperation;
use DateTime;

class DonatesController extends \yii\console\Controller
{
    const CHK_FAILED_NEXT_PAYMENT_DATE_IN_FUTURE = -1;
    const CHK_FAILED_MORE_THAN_5_DAYS = -2;
    const CHK_FAILED_LAST_PAYMENT_WAS_SUCCEEDED = -3;
    const CHK_PASSED_NO_OTHER_PAYMENTS = 1;
    const CHK_PASSED_LAST_PAYMENT_WAS_CANCELED = 2;

    public $defaultAction = 'list';


    /**
     * Показывает список последних донатов
     *
     * @param integer $limit
     * @return void
     */
    public function actionList($limit = 15)
    {
        $limit = intval($limit);
        if ($limit < 1) $limit = 1;

        $items = Donate::find()
            ->with('donor')
            ->orderBy(['id' => SORT_DESC])
            ->limit($limit)
            ->all();

        $this->writeTable(
            $items,
            [
                Donate::class => [
                    'id',
                    'donor' => function ($model) {
                        return "#$model->donorId {$model->donor->email}";
                    },
                    'amount',
                    'monthly' => function ($model) {
                        return $model->monthly ? 'yes' : 'no';
                    },
                    'enabled' => function ($model) {
                        return $model->enabled ? 'yes' : 'no';
                    },
                    'paid' => function ($model) {
                        return $model->paid ? 'yes' : 'no';
                    },
                    'last payment' => function ($model) {
                        /** @var Donate $model */
                        if (!$model->monthly) return '';
                        $payment = $model->lastPayment;
                        if (!$payment) return '?';
                        $status = $this->convertPaymentStatusToString($payment);
                        return "#$payment->id $payment->createdAt $status";
                    },
                    'createdAt',
                    'lastPaidAt',
                    'nextPaymentAt',
                ],
            ],
            Donate::find()->count()
        );
    }



    /**
     * Показывает данные заданного доната
     *
     * @param int $donateId
     * @return void
     */
    public function actionShow($donateId)
    {
        $donateId = intval($donateId);
        $item = Donate::find()
            ->with('donor')
            ->where(['id' => $donateId])
            ->one();
        if (!$item) {
            $this->writeError("Not found.");
            return;
        }
        /** @var Donate $item */
        $attrs = array_merge(
            $item->attributes,
            [
                'paid' => $item->paid,
                'isReadyForRepeat' => $item->isReadyForRepeat,
                'daysTillNextPayment' => $item->daysTillNextPayment,
            ]
        );
        $this->write(VarDumper::dumpAsString($attrs));
    }

    public function actionDisable($donateId)
    {
        $donateId = intval($donateId);
        if (DonateService::getInstance()->disableDonate($donateId)) {
            $this->writeSuccess("Done");
        } else {
            $this->writeError("Failed");
        }
    }

    public function actionEnable($donateId)
    {
        $donateId = intval($donateId);
        if (DonateService::getInstance()->enableDonate($donateId)) {
            $this->writeSuccess("Done");
        } else {
            $this->writeError("Failed");
        }
    }


    /**
     * Показывает данные заданного донора
     *
     * @param int $donorId
     * @return void
     */
    public function actionShowDonor($donorId)
    {
        $donorId = intval($donorId);
        $item = Donor::find()
            ->where(['id' => $donorId])
            ->asArray()
            ->one();
        if (!$item) {
            $this->writeError("Not found.");
            return;
        }
        $this->write(VarDumper::dumpAsString($item));
    }


    /**
     * Показывает список последних зарегистрированных доноров
     *
     * @param integer $limit
     * @return void
     */
    public function actionListDonors($limit = 15)
    {
        $limit = intval($limit);
        if ($limit < 1) $limit = 1;

        $this->writeTable(
            Donor::find()->orderBy(['id' => SORT_DESC])->limit($limit)->all(),
            [
                Donor::class => [
                    'id',
                    'name',
                    'phone',
                    'email',
                    'totalDonatesCount',
                    'totalPaymentsAmount' => function ($model) {
                        return Yii::$app->formatter->asCurrency($model->totalPaymentsAmount);
                    }
                ],
            ],
            Donor::find()->count()
        );
    }


    /**
     * Показывает данные заданного платежа
     *
     * @param int $paymentId
     * @return void
     */
    public function actionShowPayment($paymentId)
    {
        $paymentId = intval($paymentId);
        $item = Payment::find()
            ->with('paymentServiceOperation')
            ->where(['id' => $paymentId])
            ->asArray()
            ->one();
        if (!$item) {
            $this->writeError("Not found.");
            return;
        }
        if (isset($item['paymentServiceOperation']['request'])) {
            $item['paymentServiceOperation']['request'] = Json::decode($item['paymentServiceOperation']['request']);
        }
        if (isset($item['paymentServiceOperation']['response'])) {
            $item['paymentServiceOperation']['response'] = Json::decode($item['paymentServiceOperation']['response']);
        }
        $this->write(VarDumper::dumpAsString($item));
    }


    /**
     * Показывает список последних платежей
     *
     * @param integer $limit
     * @return void
     */
    public function actionListPayments($limit = 15)
    {
        $limit = intval($limit);
        if ($limit < 1) $limit = 1;

        $this->writeTable(
            Payment::find()->with('paymentServiceOperation')->orderBy(['id' => SORT_DESC])->limit($limit)->all(),
            [
                Payment::class => [
                    'id',
                    'createdAt',
                    'amount',
                    'status',
                    'error',
                    'updatedAt',
                    'operationId' => function ($model) {
                        return $model->paymentServiceOperationId;
                    },
                ],
            ],
            Payment::find()->count()
        );
    }


    /**
     * Показать историю платежей для заданного доната
     *
     * @param int $donateId
     * @return void
     */
    public function actionListDonatePayments($donateId)
    {
        $this->writeTable(
            Payment::find()
                ->with('paymentServiceOperation')
                ->where(['donateId' => intval($donateId)])
                ->orderBy(['id' => SORT_ASC])
                ->all(),
            [
                Payment::class => [
                    'id',
                    'createdAt',
                    'updatedAt',
                    'amount',
                    'status',
                    'error',
                    'notificationSent' => function ($model) {
                        return $model->notificationSent ? 'yes' : 'no';
                    },
                    'operationId' => function ($model) {
                        return $model->paymentServiceOperationId;
                    },
                ],
            ]
        );
    }


    /**
     * Показывает историю донатов заданного донора
     *
     * @param int $donorId
     * @return void
     */
    public function actionListDonorDonates($donorId)
    {
        $this->writeTable(
            Donate::find()
                ->where(['donorId' => $donorId])
                ->orderBy(['id' => SORT_ASC])
                ->all(),
            [
                Donate::class => [
                    'id',
                    'amount',
                    'monthly' => function ($model) {
                        return $model->monthly ? 'yes' : 'no';
                    },
                    'enabled' => function ($model) {
                        return $model->enabled ? 'yes' : 'no';
                    },
                    'paid' => function ($model) {
                        return $model->paid ? 'yes' : 'no';
                    },
                    'createdAt',
                    'lastPaidAt',
                    'nextPaymentAt',
                ],
            ]
        );
    }



    /**
     * Показывает список последних операций
     *
     * @param integer $limit
     * @return void
     */
    public function actionListOperations($limit = 15)
    {
        $limit = intval($limit);
        if ($limit < 1) $limit = 1;

        $this->writeTable(
            PaymentServiceOperation::find()->orderBy(['paymentId' => SORT_DESC])->limit($limit)->all(),
            [
                PaymentServiceOperation::class => [
                    'donateId',
                    'paymentId',
                    'operationId',
                    'recurrentOperationId',
                ],
            ],
            PaymentServiceOperation::find()->count()
        );
    }


    /**
     * Показывает список ежемесячных донатов, платежи по которым будут производиться в течении следующих 20 дней.
     *
     * @return void
     */
    public function actionListNextMonthly()
    {
        $items = Donate::queryAllEnabled()
            ->select('*, DATEDIFF(`nextPaymentAt`, NOW()) d')
            ->andWhere(['monthly' => true])
            ->having('d >= 0 AND d < 20')
            ->orderBy(['nextPaymentAt' => SORT_ASC])
            ->all();

        $this->writeTable(
            $items,
            [
                Donate::class => [
                    'id',
                    'createdAt',
                    'donor' => function ($model) {
                        return "#$model->donorId {$model->donor->email}";
                    },
                    'amount',
                    'last payment' => function ($model) {
                        /** @var Donate $model */
                        if (!$model->monthly) return '';
                        $payment = $model->lastPayment;
                        if (!$payment) return '?';
                        $status = $this->convertPaymentStatusToString($payment);
                        return "#$payment->id $payment->createdAt $status";
                    },
                    'nextPaymentAt',
                ],
            ]
        );
        return;
    }



    public function actionListFailedMonthly()
    {
        //TODO
    }


    /**
     * Проводит автоплатёж заданного ежемесячного доната.
     *
     * @param int|string $donateId
     * @return void
     */
    public function actionRepeat($donateId)
    {
        $psParams = DonateService::getInstance()->paymentService->getServiceParams();
        $this->write('Payment service params: ' . VarDumper::dumpAsString($psParams));

        $donate = Donate::findOne($donateId);
        if (!$donate) {
            $this->writeWarning('Not found.');
            return;
        }
        if (!$donate->monthly) {
            $this->writeWarning('This donate is not monthly!');
            return;
        }
        $this->write('Donate data: ' . VarDumper::dumpAsString($donate->attributes));

        if (!$this->confirm('Do you want to make payment?')) {
            return;
        }

        $this->write('sending request... ', false);
        DbSessionSettings::setWaitTimeout(120);
        $payment = DonateService::getInstance()->repeatPayment($donate);
        DbSessionSettings::restoreWaitTimeout();
        $this->write('ok');

        if (!$payment) {
            $this->writeError('Failed to repeat payment.');
            return;
        }

        if ($payment->isPending) {
            $this->write('retrieving payment status... ', false);
            sleep(1);
            $payment->refresh();
            $response = DonateService::getInstance()->queryPaymentInfo($payment);
            if ($response) {
                DonateService::getInstance()->updatePaymentStatus($response);
                $payment->refresh();
            }
            $this->write('ok');
        }

        if ($payment->isSucceeded) {
            $this->writeSuccess("Payment done:");
        } else if ($payment->isPending) {
            $this->writeWarning('Payment still pending');
        } else {
            $this->writeError('Failed to make payment due to: ' . $payment->error);
        }

        $this->write(" id: $payment->id");
        $this->write(" amount: $payment->amount");
        $this->write(" operationId: $payment->paymentServiceOperationId");

        if (!$payment->isPending) {
            if ($this->confirm("Send notify to user {$payment->donate->donor->email} ?")) {
                $this->write('sending... ', false);
                if ($payment->isSucceeded) {
                    $sent = DonateService::getInstance()->notificationService->notifyAfterSuccededNextReccuringPayment($payment);
                } else if ($payment->isCanceled) {
                    $sent = DonateService::getInstance()->notificationService->notifyAfterFailedNextReccuringPayment($payment);
                }
                if ($sent) {
                    $this->write('ok');
                    $payment->markAsNotificationSentAndSave();
                } else {
                    $this->writeError('failed');
                }
            }
        }
    }


    /**
     * Выдаёт список ежемесячных донатов, по которым можно запросить платежи СЕГОДНЯ. 
     * Эти донаты будут обработаны командой /donates/repaet-all
     *
     * @return void
     */
    public function actionListDonatesReadyForRepeat()
    {
        $items = Donate::queryAllEnabled()
            ->select('*, DATEDIFF(`nextPaymentAt`, NOW()) d')
            ->andWhere(['monthly' => true])
            ->having('d <= 0')
            ->orderBy(['nextPaymentAt' => SORT_ASC])
            ->all();

        $this->writeTable(
            $items,
            [
                Donate::class => [
                    'id',
                    'createdAt',
                    'donor' => function ($model) {
                        return "#$model->donorId {$model->donor->email}";
                    },
                    'amount',
                    'last payment' => function ($model) {
                        /** @var Donate $model */
                        if (!$model->monthly) return '';
                        $payment = $model->lastPayment;
                        if (!$payment) return '?';
                        $status = $this->convertPaymentStatusToString($payment);
                        return "#$payment->id $payment->createdAt $status";
                    },
                    'nextPaymentAt',
                    'lastPaidAt',
                ],
            ]
        );
    }


    /**
     * Проводит платежи по всем ежемесячным донатам, платежи по которым запланированы на сегодня либо не прошли в предыдущии дни.
     *
     * @param boolean $testing
     * @return void
     */
    public function actionRepeatAll()
    {
        $donates = Donate::queryAllEnabled()
            ->select('*, DATEDIFF(`nextPaymentAt`, NOW()) d')
            ->andWhere(['monthly' => true])
            ->having('d <= 0')
            ->orderBy(['nextPaymentAt' => SORT_ASC])
            ->all();
        if (!$donates) {
            Yii::info('No donates to repeat');
            return;
        }

        /** @var Donate[] $donates */
        DbSessionSettings::setWaitTimeout(120);
        foreach ($donates as $donate) {
            Yii::info("Try to repeat donate id=$donate->id");
            Yii::debug($donate->attributes);

            $created = mb_substr($donate->createdAt, 0, 10);
            $next = mb_substr($donate->nextPaymentAt, 0, 10);
            $message = "#$donate->id\t$created/$next";

            $canRepeat = $this->checkDonateRepetitionPossibility($donate);
            if ($canRepeat > 0) {
                $this->write("$message\t[$canRepeat]\t> repeat... ", false);
            } else {
                $err = $this->getHumanReadableCheckResult($canRepeat);
                $this->write("$message\t> skip, due to '$err'");
                Yii::info("Skip donate id=$donate->id");
                if ($canRepeat == -2) {
                    $donate->calculateNextPaymentDateForNextMonth($donate->createdAt);
                    if (!$donate->save()) {
                        Yii::error('Failed to update Donate');
                        Yii::error($donate->errors);
                        Yii::info($donate->attributes);
                    }
                }
                continue;
            }
            $payment = DonateService::getInstance()->repeatPayment($donate);
            if (!$payment) {
                $this->writeError('failed');
                Yii::error('Failed to repeat donate payment');
                Yii::info($donate->attributes);
                continue;
            }
            $this->writeSuccess('done;', false);
            $this->write(' sending message...', false);
            Yii::debug($payment->attributes);
            if ($payment->isSucceeded) {
                $this->writeSuccess('done');
                $sent = DonateService::getInstance()->notificationService->notifyAfterSuccededNextReccuringPayment($payment);
            } else if ($payment->isCanceled) {
                $this->writeError('failed');
                $sent = DonateService::getInstance()->notificationService->notifyAfterFailedNextReccuringPayment($payment);
            }
            if ($sent) {
                $payment->markAsNotificationSentAndSave();
            }
        }
        DbSessionSettings::restoreWaitTimeout();
    }

    protected function getHumanReadableCheckResult(int $result): string
    {
        switch ($result) {
            case self::CHK_FAILED_NEXT_PAYMENT_DATE_IN_FUTURE:
                return 'Донат неактивен или дата платежа в будущем';
            case self::CHK_FAILED_MORE_THAN_5_DAYS:
                return 'С последнего платежа прошло более 5 дней';
            case self::CHK_FAILED_LAST_PAYMENT_WAS_SUCCEEDED:
                return 'Последний платёж был недавно и он успешен';
            case self::CHK_PASSED_NO_OTHER_PAYMENTS:
                return 'Это первый платёж';
            case self::CHK_PASSED_LAST_PAYMENT_WAS_CANCELED:
                return 'Последний платёж был недавно и он не прошёл';
            default:
                return '?';
        }
    }

    /**
     * Проверка возможности проведения повторного платежа по донату
     *
     * @param Donate $donate
     * @return integer Отрицательное - повтор не возможен, положительное - возможен
     */
    protected function checkDonateRepetitionPossibility(Donate $donate): int
    {
        if (!$donate->isReadyForRepeat) return self::CHK_FAILED_NEXT_PAYMENT_DATE_IN_FUTURE;

        $edgePayment = Payment::find()
            ->where(['donateId' => $donate->id])
            ->orderBy(['id' => SORT_DESC])
            ->limit(5)
            ->one();
        /** @var Payment $edgePayment */
        if (!$edgePayment) return self::CHK_PASSED_NO_OTHER_PAYMENTS;

        $now = new DateTime();
        $edgePaymentDate = new DateTime($edgePayment->createdAt);
        if ($now->diff($edgePaymentDate)->days >= 5) return self::CHK_FAILED_MORE_THAN_5_DAYS;

        if ($donate->lastPayment->isCanceled) return self::CHK_PASSED_LAST_PAYMENT_WAS_CANCELED;
        return self::CHK_FAILED_LAST_PAYMENT_WAS_SUCCEEDED;
    }

    //---------------------



    protected function writeError($msg, $newLine = true)
    {
        if (is_array($msg)) {
            $msg = join("\n", $msg);
        }
        $eol = $newLine ? "\n" : '';
        $this->stdout($msg . $eol, Console::FG_RED);
    }

    protected function writeWarning($msg, $newLine = true)
    {
        $eol = $newLine ? "\n" : '';
        $this->stdout($msg . $eol, Console::FG_YELLOW);
    }

    protected function writeSuccess($msg, $newLine = true)
    {
        $eol = $newLine ? "\n" : '';
        $this->stdout($msg . $eol, Console::FG_GREEN);
    }

    protected function write($msg, $newLine = true)
    {
        $eol = $newLine ? "\n" : '';
        $this->stdout($msg . $eol);
    }

    protected function confirmDangerAction($msg, $default = false): bool
    {
        $msg = Console::ansiFormat($msg . ' (yes|no) [' . ($default ? 'yes' : 'no') . ']:', [Console::FG_RED]);
        while (true) {
            Console::stdout($msg);
            $input = trim(Console::stdin());

            if (empty($input)) {
                return $default;
            }

            if (!strcasecmp($input, 'y') || !strcasecmp($input, 'yes')) {
                return true;
            }

            if (!strcasecmp($input, 'n') || !strcasecmp($input, 'no')) {
                return false;
            }
        }
    }

    protected function writeTable(array $models, array $toArrayConfig, int $totalCount = -1, array $columns = [])
    {
        if (empty($models)) {
            $this->writeWarning('Not found');
            return;
        }
        $items = ArrayHelper::toArray($models, $toArrayConfig);
        if (empty($items)) {
            $this->write('No data');
            return;
        }

        if (empty($columns)) { //autodetect
            reset($toArrayConfig);
            foreach (current($toArrayConfig) as $key => $value) {
                if (is_int($key) && is_string($value)) {
                    $columns[] = $value;
                } else if (is_string($key)) {
                    $columns[] = $key;
                }
            }
        }

        echo Table::widget([
            'headers' =>  $columns,
            'rows' => $items,
        ]);

        $listed = count($items);
        $output = "Listed $listed items";
        if ($totalCount >= 0) $output .= " of $totalCount.";
        $this->write($output);
    }

    protected function convertPaymentStatusToString(?Payment $payment): string
    {
        if (!$payment) return '';
        if ($payment->isSucceeded) {
            return 'paid';
        } else if ($payment->isCanceled) {
            return 'failed';
        } else if ($payment->isPending) {
            return 'pending';
        }
        return 'uknown';
    }
}
