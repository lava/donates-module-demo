<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%payment_service_operations}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%donates}}`
 * - `{{%payments}}`
 */
class m210127_115406_create_payment_service_operations_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%payment_service_operations}}', [
            'donateId' => $this->integer()->notNull(),
            'paymentId' => $this->integer()->notNull(),
            'operationId' => $this->string()->notNull(),
            'recurrentOperationId' => $this->string(),
            'request' => $this->text(),
            'response' => $this->text(),
        ]);

        $this->createIndex(
            '{{%idx-payment_service_operations-recurrentOperationId}}',
            '{{%payment_service_operations}}',
            'recurrentOperationId'
        );

        // creates index for column `donateId`
        $this->createIndex(
            '{{%idx-payment_service_operations-donateId}}',
            '{{%payment_service_operations}}',
            'donateId'
        );

        // add foreign key for table `{{%donates}}`
        $this->addForeignKey(
            '{{%fk-payment_service_operations-donateId}}',
            '{{%payment_service_operations}}',
            'donateId',
            '{{%donates}}',
            'id',
            'CASCADE'
        );

        // creates index for column `paymentId`
        $this->createIndex(
            '{{%idx-payment_service_operations-paymentId}}',
            '{{%payment_service_operations}}',
            'paymentId',
            true
        );

        // add foreign key for table `{{%payments}}`
        $this->addForeignKey(
            '{{%fk-payment_service_operations-paymentId}}',
            '{{%payment_service_operations}}',
            'paymentId',
            '{{%payments}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%donates}}`
        $this->dropForeignKey(
            '{{%fk-payment_service_operations-donateId}}',
            '{{%payment_service_operations}}'
        );

        // drops index for column `donateId`
        $this->dropIndex(
            '{{%idx-payment_service_operations-donateId}}',
            '{{%payment_service_operations}}'
        );

        // drops foreign key for table `{{%payments}}`
        $this->dropForeignKey(
            '{{%fk-payment_service_operations-paymentId}}',
            '{{%payment_service_operations}}'
        );

        // drops index for column `paymentId`
        $this->dropIndex(
            '{{%idx-payment_service_operations-paymentId}}',
            '{{%payment_service_operations}}'
        );

        $this->dropTable('{{%payment_service_operations}}');
    }
}
