<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%donates}}`.
 */
class m210115_113548_create_donates_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%donates}}', [
            'id' => $this->primaryKey(),
            'categoryId' => $this->integer()->notNull(),
            'hash' => $this->string(64)->notNull(),
            'donorId' => $this->integer()->notNull(),
            'createdAt' => $this->datetime()->notNull(),
            'updatedAt' => $this->datetime()->defaultExpression('NOW()'),
            'amount' => $this->integer()->notNull(),
            'anonymous' => $this->boolean()->notNull()->defaultValue(false),
            'monthly' => $this->boolean()->notNull()->defaultValue(false),
            'enabled' => $this->boolean()->notNull()->defaultValue(true),
            'nextPaymentAt' => $this->dateTime(),
        ]);

        $this->createIndex('{{%idx-donates-categoryId}}', '{{%donates}}', 'categoryId');
        $this->createIndex('{{%idx-donates-hash}}', '{{%donates}}', 'hash');
        $this->createIndex('{{%idx-donates-monthly}}', '{{%donates}}', 'monthly');
        $this->createIndex('{{%idx-donates-enabled}}', '{{%donates}}', 'enabled');
        $this->createIndex('{{%idx-donates-createdAt}}', '{{%donates}}', 'createdAt');
        $this->createIndex('{{%idx-donates-nextPaymentAt}}', '{{%donates}}', 'nextPaymentAt');

        $this->createIndex(
            '{{%idx-donates-donorId}}',
            '{{%donates}}',
            'donorId'
        );

        // add foreign key for table `{{%donors}}`
        $this->addForeignKey(
            '{{%fk-donates-donorId}}',
            '{{%donates}}',
            'donorId',
            '{{%donors}}',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            '{{%fk-donates-categoryId}}',
            '{{%donates}}',
            'categoryId',
            '{{%donate_categories}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            '{{%fk-donates-donorId}}',
            '{{%donates}}'
        );

        $this->dropForeignKey(
            '{{%fk-donates-categoryId}}',
            '{{%donates}}'
        );

        $this->dropTable('{{%donates}}');
    }
}
