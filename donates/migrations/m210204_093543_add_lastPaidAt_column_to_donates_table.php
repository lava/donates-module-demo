<?php

use app\modules\donates\models\Donate;
use app\modules\donates\models\PaymentServiceOperation;
use yii\db\Migration;
use yii\helpers\Json;

/**
 * Handles adding columns to table `{{%donates}}`.
 */
class m210204_093543_add_lastPaidAt_column_to_donates_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%donates}}', 'lastPaidAt', $this->datetime());

        //fill field
        foreach (Donate::find()->each(20) as $donate) {
            /** @var Donate $donate */
            if ($donate->monthly) {
                $response = PaymentServiceOperation::find()
                    ->select('response')
                    ->where(['donateId' => $donate->id])
                    ->orderBy(['paymentId' => SORT_DESC])
                    ->limit(1)
                    ->scalar();
                if ($response) {
                    $response = Json::decode($response);
                    if (isset($response['created_at']) && !empty($response['created_at'])) {
                        $date = new DateTime($response['created_at']);
                        $donate->lastPaidAt = $date->format('Y-m-d H:i:s');
                    }
                }
            } else {
                $donate->lastPaidAt = $donate->createdAt;
            }
            if (!$donate->save()) {
                return false;
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%donates}}', 'lastPaidAt');
    }
}
