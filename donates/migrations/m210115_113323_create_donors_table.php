<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%donors}}`.
 */
class m210115_113323_create_donors_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%donors}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'phone' => $this->string(),
            'email' => $this->string()->notNull()->unique(),
        ]);

        $this->createIndex('{{%idx-donors-phone}}', '{{%donors}}', 'phone');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%donors}}');
    }
}
