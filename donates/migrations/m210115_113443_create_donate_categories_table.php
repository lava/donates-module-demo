<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%donate_categories}}`.
 */
class m210115_113443_create_donate_categories_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%donate_categories}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
        ]);

        $this->insert('{{%donate_categories}}', ['name' => 'На нужды приюта']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%donate_categories}}');
    }
}
