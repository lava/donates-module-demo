<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%payments}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%donates}}`
 */
class m210115_115645_create_payments_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%payments}}', [
            'id' => $this->primaryKey(),
            'donateId' => $this->integer(),
            'type' => 'ENUM("single","monthly-first","monthly-next") NOT NULL',
            'status' => 'ENUM("succeeded","pending","canceled") NOT NULL',
            'amount' => $this->integer()->notNull(),
            'error' => $this->string(),
            'createdAt' => $this->datetime()->notNull(),
            'updatedAt' => $this->datetime()->defaultExpression('NOW()'),
            'notificationSent' => $this->boolean()->notNull()->defaultValue(false),
        ]);

        $this->createIndex('{{%idx-payments-notificationSent}}', '{{%payments}}', 'notificationSent');
        $this->createIndex('{{%idx-payments-type}}', '{{%payments}}', 'type');
        $this->createIndex('{{%idx-payments-status}}', '{{%payments}}', 'status');
        $this->createIndex('{{%idx-payments-createdAt}}', '{{%payments}}', 'createdAt');

        // creates index for column `donateId`
        $this->createIndex(
            '{{%idx-payments-donateId}}',
            '{{%payments}}',
            'donateId'
        );

        // add foreign key for table `{{%donates}}`
        $this->addForeignKey(
            '{{%fk-payments-donateId}}',
            '{{%payments}}',
            'donateId',
            '{{%donates}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%donates}}`
        $this->dropForeignKey(
            '{{%fk-payments-donateId}}',
            '{{%payments}}'
        );

        // drops index for column `donateId`
        $this->dropIndex(
            '{{%idx-payments-donateId}}',
            '{{%payments}}'
        );

        $this->dropTable('{{%payments}}');
    }
}
