<?php

namespace app\modules\donates\widgets;

use yii\bootstrap4\Widget;
use app\modules\donates\models\Donor;
use app\modules\donates\models\Payment;

class BestDonors extends Widget
{
    const ORDER_BY_DONATES_COUNT = 1;
    const ORDER_BY_TOTAL_PAYMENTS_AMOUNT = 2;
    /** 
     * Items to display.
     * 
     * @var \yii\data\ActiveDataProvider 
     */
    public $dataProvider = null;

    /**
     * Items to display. Used if $this->dataProvider is empty.
     * If $this->donates not set, it will be autoloaded.
     *
     * @var Donate[]
     */
    public $items = [];


    public $limit = 10;


    public function run()
    {
        if (!empty($this->dataProvider)) {
            $this->items = $this->dataProvider->limit($this->limit)->all();
        } else if (empty($this->items)) {
            $this->items = Donor::find()
                ->joinWith('donates.payments')
                ->select('donors.*, COUNT(donates.id) AS totalDonatesCount, SUM(payments.amount) AS totalPaymentsAmount')
                ->where(['payments.status' => Payment::STATUS_SUCCEEDED])
                ->groupBy('donors.id')
                ->orderBy(['totalPaymentsAmount' => SORT_DESC, 'totalDonatesCount' => SORT_DESC])
                ->limit($this->limit)
                ->all();
        }

        return $this->renderFile(
            __DIR__ . DIRECTORY_SEPARATOR . 'BestDonors' . DIRECTORY_SEPARATOR . 'view.php',
            [
                'donors' => $this->items,
            ]
        );
    }
}
