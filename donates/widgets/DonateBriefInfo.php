<?php

namespace app\modules\donates\widgets;

use app\modules\donates\components\DonateService;
use InvalidArgumentException;

use Yii;
use yii\bootstrap4\Html;
use yii\bootstrap4\Widget;

use app\modules\donates\models\Donate;

class DonateBriefInfo extends Widget
{
    /**
     * Donate or Donate's ID
     *
     * @var int|string|Donate
     */
    public $donate;

    public $asLink = false;

    public $linkOptions = [];

    public $showStatus = true;
    public $showErrorDescription = false;

    public $notFoundMessage = 'Не найдено';

    public function run()
    {
        $this->loadDonate();
        if (empty($this->donate)) {
            return $this->notFoundMessage;
        }

        Html::addCssClass($this->options, 'donate-brief-info-widget');
        $html = Html::beginTag('div', $this->options);
        $info = $this->renderInfo();
        if ($this->asLink) {
            $html .= Html::a($info, ['donate-details', 'id' => $this->donate->id], $this->linkOptions);
        } else {
            $html .= $info;
        }
        if ($this->showStatus) {
            $html .= $this->renderStatus();
        }
        $html .= Html::endTag('div');
        return $html;
    }

    protected function renderInfo(): string
    {
        $date = Yii::$app->formatter->asDatetime($this->donate->createdAt);
        $amount = Yii::$app->formatter->asCurrency($this->donate->amount);
        $html = Html::tag(
            'span',
            "#{$this->donate->id}",
            ['class' => 'donate-brief-info-widget']
        );
        $html .= Html::tag(
            'span',
            " от $date",
            ['class' => 'donate-brief-info-widget__date']
        );
        $html .= Html::tag(
            'span',
            " на $amount",
            ['class' => 'donate-brief-info-widget__amount']
        );
        return $html;
    }

    protected function renderStatus(): string
    {
        $error = $this->showErrorDescription ? $this->renderErrorDescription() : '';

        return Html::tag(
            'div',
            StatusIcon::widget([
                'item' => $this->donate,
                'showText' => true
            ]) . ' ' . $error,
            ['class' => 'donate-brief-info-widget__status']
        );
    }

    protected function renderErrorDescription(): string
    {
        if ($this->payment->isSucceeded || $this->payment->isPending) {
            return '';
        }
        $error = DonateService::getInstance()->getPaymentErrorDescription($this->payment->error);
        return Html::tag(
            'div',
            Html::tag('small', $error),
            ['class' => '']
        );
    }

    protected function loadDonate()
    {
        if (empty($this->donate)) {
            throw new InvalidArgumentException("Donate not set");
        }
        if (is_numeric($this->donate)) {
            $this->donate = Donate::findOne($this->donate);
        }

        if (!($this->donate instanceof Donate)) {
            throw new InvalidArgumentException("Invalid Donate");
        }
    }
}
