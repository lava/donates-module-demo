<?php

namespace app\modules\donates\widgets;

use yii\bootstrap4\Widget;
use app\modules\donates\models\Donate;

class NextMonthlyDonates extends Widget
{
    /** 
     * Items to display.
     * 
     * @var \yii\data\ActiveDataProvider 
     */
    public $dataProvider = null;

    /**
     * Items to display. Used if $this->dataProvider is empty.
     * If $this->donates not set, it will be autoloaded.
     *
     * @var Donate[]
     */
    public $items = [];

    /**
     * Term in days.
     *
     * @var integer
     */
    public $term = 20;

    public $limit = 15;

    /**
     * Shoud return array of items like ['label'=>...,'url'=>...]
     *
     * @var callable|null
     */
    public $actions = null;
    // public $repeatPaymentUrlFormatter = null;

    // public $sendNotificationBeforePaymentUrlFormatter = null;


    public function run()
    {
        if (!empty($this->dataProvider)) {
            $this->items = $this->dataProvider->limit($this->limit)->all();
        } else if (empty($this->items)) {
            $this->items = Donate::queryAllEnabled()
                ->select('*, DATEDIFF(`nextPaymentAt`, NOW()) daysTillNextPayment')
                ->andWhere(['monthly' => true])
                ->having('daysTillNextPayment < ' . intval($this->term))
                ->orderBy(['nextPaymentAt' => SORT_ASC])
                ->limit($this->limit)
                ->all();
        }



        return $this->renderFile(
            __DIR__ . DIRECTORY_SEPARATOR . 'NextMonthlyDonates' . DIRECTORY_SEPARATOR . 'view2.php',
            [
                'donates' => $this->items,
            ]
        );
    }

    public function getActions(Donate $donate)
    {
        if (is_callable($this->actions)) {
            return call_user_func($this->actions, $donate);
        }
        return [];
    }
}
