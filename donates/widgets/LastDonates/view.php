<?php

/** @var yii\web\View $this */
/** @var app\modules\donates\models\Donate[] $donates */

use app\modules\donates\components\DonateService;
use app\modules\donates\models\Donate;
use app\modules\donates\models\Payment;
use app\modules\donates\widgets\DonorNameAndContacts;
use yii\bootstrap4\Html;

function getClassByDonatePaymentStatus(Donate $donate): string
{
    $statusClasses = [
        Payment::STATUS_CANCELED => 'text-danger',
        Payment::STATUS_SUCCEEDED => '',
        Payment::STATUS_PENDING => '',
    ];

    if ($donate->lastPayment && isset($statusClasses[$donate->lastPayment->status])) {
        return $statusClasses[$donate->lastPayment->status];
    }
    return '';
}


function renderStatus(Donate $donate): string
{
    //TODO use StatusIcon widget
    $html = '';
    $status = '';
    $error = '';
    if ($donate->lastPayment) {
        if ($donate->lastPayment->isSucceeded) {
            $status = 'Оплачено';
        } else if ($donate->lastPayment->isPending) {
            $status = 'Ожидание платежа';
        } else {
            $status = 'Платёж не прошёл';
            $error = DonateService::getInstance()->getPaymentErrorDescription($donate->lastPayment->error);
        }
    }
    $html .= Html::tag('p', $status, ['class' => 'last-donates-widget__status-paid']);
    if (!empty($error)) {
        $html .= Html::tag('p', $error, ['class' => 'last-donates-widget__status-error']);
    }
    return $html;
}

?>
<table class="table table-sm table-striped table-bordered last-donates-widget">
    <thead class="thead-dark">
        <tr>
            <th>id</th>
            <th>Дата</th>
            <th>Сумма</th>
            <th>Статус</th>
            <th>Донор</th>
        </tr>
    </thead>
    <tbody>
        <?php if ($donates) : ?>
            <?php foreach ($donates as $donate) : ?>
                <tr class="<?= getClassByDonatePaymentStatus($donate) ?>">
                    <td class="last-donates-widget__id">
                        <?= $donate->id ?>
                    </td>
                    <td class="last-donates-widget__date">
                        <?= Yii::$app->formatter->asDatetime($donate->createdAt) ?>
                    </td>
                    <td class="last-donates-widget__amount">
                        <?= Yii::$app->formatter->asCurrency($donate->amount) ?>
                        <?php if ($donate->monthly) : ?>
                            <div class="mt-1 text-info"><small>ежемесячно</small></div>
                        <?php endif; ?>
                    </td>
                    <td class="last-donates-widget__status">
                        <?= renderStatus($donate) ?>

                    </td>
                    <td class="last-donates-widget__donor">
                        <?= DonorNameAndContacts::widget(['donor' => $donate->donor, 'multiLine' => true]) ?>
                    </td>
                </tr>
            <?php endforeach; ?>
        <?php endif; ?>
    </tbody>
</table>