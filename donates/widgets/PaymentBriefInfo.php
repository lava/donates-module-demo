<?php

namespace app\modules\donates\widgets;

use InvalidArgumentException;

use Yii;
use yii\bootstrap4\Html;
use yii\bootstrap4\Widget;

use app\modules\donates\components\DonateService;
use app\modules\donates\models\Payment;

class PaymentBriefInfo extends Widget
{
    /**
     * Payment or Payment's ID
     *
     * @var int|string|Payment
     */
    public $payment;

    public $asLink = false;

    public $showStatus = true;
    public $showErrorDescription = false;

    public $linkOptions = [];

    public $notFoundMessage = 'Не найдено';

    public function run()
    {
        $this->loadPayment();
        if (empty($this->payment)) {
            return $this->notFoundMessage;
        }

        Html::addCssClass($this->options, 'payment-brief-info-widget my-2');
        $html = Html::beginTag('div', $this->options);
        $info = $this->renderInfo();
        if ($this->asLink) {
            $this->linkOptions['title'] = $this->linkOptions['title'] ?? 'Подробнее';
            $html .= Html::a(
                $info,
                ['payment-details', 'id' => $this->payment->id],
                $this->linkOptions
            );
        } else {
            $html .= $info;
        }
        if ($this->showStatus) {
            $html .= $this->renderStatus();
        }
        $html .= Html::endTag('div');
        return $html;
    }

    protected function renderInfo(): string
    {
        $date = Yii::$app->formatter->asDatetime($this->payment->createdAt);
        $amount = Yii::$app->formatter->asCurrency($this->payment->amount);
        $html = Html::tag(
            'span',
            "#{$this->payment->id}",
            ['class' => 'payment-brief-info-widget__id font-weight-bold']
        );
        $html .= Html::tag(
            'span',
            " от $date",
            ['class' => 'payment-brief-info-widget__date']
        );
        $html .= Html::tag(
            'span',
            " на $amount",
            ['class' => 'payment-brief-info-widget__amount']
        );
        return $html;
    }

    protected function renderStatus(): string
    {
        $error = $this->showErrorDescription ? $this->renderErrorDescription() : '';

        return Html::tag(
            'div',
            StatusIcon::widget([
                'item' => $this->payment,
                'showText' => true
            ]) . ' ' . $error,
            ['class' => 'payment-brief-info-widget__status']
        );
    }

    protected function renderErrorDescription(): string
    {
        if ($this->payment->isSucceeded || $this->payment->isPending) {
            return '';
        }
        $error = DonateService::getInstance()->getPaymentErrorDescription($this->payment->error);
        return Html::tag(
            'div',
            Html::tag('small', $error),
            ['class' => '']
        );
    }

    protected function loadPayment()
    {
        if (empty($this->payment)) {
            throw new InvalidArgumentException("Payment not set");
        }
        if (is_numeric($this->payment)) {
            $this->payment = Payment::findOne($this->payment);
        }

        if (!($this->payment instanceof Payment)) {
            throw new InvalidArgumentException("Invalid Payment");
        }
    }
}
