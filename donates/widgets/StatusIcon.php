<?php

namespace app\modules\donates\widgets;

use InvalidArgumentException;

use Yii;
use yii\bootstrap4\Html;
use yii\bootstrap4\Widget;

use app\modules\donates\models\Donate;
use app\modules\donates\models\Payment;

class StatusIcon extends Widget
{
    /**
     * Donate or Payment to display status
     *
     * @var Donate|Payment
     */
    public $item;

    public $showIcon = true;
    public $showText = false;

    public $template = '{icon} {text}';



    public function run()
    {
        if (!$this->item) {
            throw new InvalidArgumentException('Empty item');
        }

        if ($this->item instanceof Donate) {
            return $this->renderDonateStatus();
        } elseif ($this->item instanceof Payment) {
            return $this->renderPaymentStatus($this->item);
        }

        throw new InvalidArgumentException('Invalid item type');
    }

    protected function renderDonateStatus(): string
    {
        $payment = $this->item->lastPayment;
        if ($payment) {
            return $this->renderPaymentStatus($payment);
        }
        Yii::warning('Donate has no payments');
        return '?';
    }

    protected function renderPaymentStatus(Payment $payment): string
    {
        $icon = $this->showIcon ? $this->renderIcon($payment) : '';
        $text = $this->showText ? $this->renderText($payment) : '';
        $out = preg_replace(
            ['/\{icon\}/', '/\{text\}/'],
            [$icon, $text],
            $this->template
        );
        return $out;
        // return "$icon $text";
    }

    protected function renderIcon(Payment $payment): string
    {
        $iconClasses = [
            Payment::STATUS_CANCELED => 'text-danger fas fa-ban',
            Payment::STATUS_SUCCEEDED => 'text-success fas fa-check',
            Payment::STATUS_PENDING => 'text-info fas fa-clock-o',
        ];
        if (isset($iconClasses[$payment->status])) {
            return Html::tag('i', '', ['class' => $iconClasses[$payment->status]]);
        }
        return '';
    }

    protected function renderText(Payment $payment): string
    {
        $textClasses = [
            Payment::STATUS_CANCELED => 'text-danger',
            Payment::STATUS_SUCCEEDED => 'text-success',
            Payment::STATUS_PENDING => 'text-info',
        ];
        if (isset($textClasses[$payment->status])) {
            return Html::tag('span', $payment->statusDesc, ['class' => $textClasses[$payment->status]]);
        }
        return '';
    }
}
