<?php

namespace app\modules\donates\widgets;

use yii\bootstrap4\Widget;

class Phonenumber extends Widget
{
    public $phone = '';

    public function run()
    {
        if (!$this->phone) return '';
        return $this->formatPhoneNumber($this->phone);
    }

    /**
     * @source https://stackoverflow.com/questions/4708248/formatting-phone-numbers-in-php
     */
    protected function formatPhoneNumber(string $phone): string
    {
        $phoneNumber = preg_replace('/[^0-9]/', '', $phone);

        if (strlen($phoneNumber) > 10) {
            $countryCode = substr($phoneNumber, 0, strlen($phoneNumber) - 10);
            if ($countryCode == 8) $countryCode = 7;
            $areaCode = substr($phoneNumber, -10, 3);
            $nextThree = substr($phoneNumber, -7, 3);
            $lastFour = substr($phoneNumber, -4, 4);
            $phoneNumber = '+' . $countryCode . ' (' . $areaCode . ') ' . $nextThree . '-' . $lastFour;
        } else if (strlen($phoneNumber) == 10) {
            $areaCode = substr($phoneNumber, 0, 3);
            $nextThree = substr($phoneNumber, 3, 3);
            $lastFour = substr($phoneNumber, 6, 4);
            $phoneNumber = '(' . $areaCode . ') ' . $nextThree . '-' . $lastFour;
        } else if (strlen($phoneNumber) == 7) {
            $nextThree = substr($phoneNumber, 0, 3);
            $lastFour = substr($phoneNumber, 3, 4);
            $phoneNumber = $nextThree . '-' . $lastFour;
        } else {
            $phoneNumber = $phone;
        }

        return $phoneNumber ?? $phone;
    }
}
