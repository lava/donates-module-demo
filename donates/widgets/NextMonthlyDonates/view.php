<?php

/** @var yii\web\View $this */
/** @var app\modules\donates\models\Donate[] $donates */

use app\modules\donates\components\DonateService;
use app\modules\donates\models\Donate;
use app\modules\donates\models\Payment;
use app\modules\donates\widgets\DonorNameAndContacts;
use yii\bootstrap4\Html;

function getRowClass(Donate $donate): string
{
    if ($donate->daysTillNextPayment < 0) {
        return 'table-warning';
    } else if ($donate->daysTillNextPayment == 0) {
        return 'table-success';
    }
    return '';
}

function renderPayment(?Payment $payment): string
{
    if (!$payment) {
        return '';
    }
    $date = $payment->updatedAt > $payment->createdAt ? $payment->updatedAt : $payment->createdAt;
    $html = Html::tag('p', Yii::$app->formatter->asDatetime($date));
    $status = '';
    $error = '';
    if ($payment->isSucceeded) {
        $status = 'Оплачено';
    } else if ($payment->isPending) {
        $status = 'Ожидание платежа';
    } else {
        $status = 'Платёж не прошёл';
        $error = DonateService::getInstance()->getPaymentErrorDescription($payment->error);
    }
    $html .= Html::tag('p', $status, ['class' => '']);
    if (!empty($error)) {
        $html .= Html::tag('p', $error, ['class' => '']);
    }
    return $html;
}


function renderActions(array $actions): string
{
    $options = ['class' => 'btn btn-sm btn-outline-dark'];
    $html = '';
    foreach ($actions as $action) {
        $html .= Html::beginTag('div', ['class' => 'text-nowrap']);
        $html .= Html::a(
            $action['label'],
            $action['url'],
            array_merge($options, $actions['options'] ?? [])
        );
        // $html .= '<br/>';
        $html .= Html::endTag('div');
    }
    return $html;
}

?>
<table class="table table-sm table-bordered next-monthly-donates-widget">
    <thead class="thead-dark">
        <tr>
            <th>id</th>
            <th>Ожидаемая дата платежа</th>
            <th>Сумма</th>
            <th>Донор</th>
            <th>Последний платёж</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($donates as $donate) : ?>
            <tr class="<?= getRowClass($donate) ?>">
                <td class="next-monthly-donates-widget__id">
                    <?= $donate->id ?>
                </td>
                <td class="next-monthly-donates-widget__date">
                    <?= Yii::$app->formatter->asDate($donate->nextPaymentAt) ?>

                </td>
                <td class="next-monthly-donates-widget__amount">
                    <?= Yii::$app->formatter->asCurrency($donate->amount) ?>
                </td>
                <td class="next-monthly-donates-widget__donor">
                    <?= DonorNameAndContacts::widget(['donor' => $donate->donor, 'multiLine' => true]) ?>
                </td>
                <td class="next-monthly-donates-widget__last-payment">
                    <?= renderPayment($donate->lastPayment) ?>
                </td>
                <td class="next-monthly-donates-widget__actions">
                    <?= renderActions($this->context->getActions($donate)) ?>
                </td>
            </tr>
        <?php endforeach ?>
    </tbody>
</table>