<?php

/** @var yii\web\View $this */
/** @var app\modules\donates\models\Donate[] $donates */

use yii\bootstrap4\Html;

use app\modules\donates\widgets\ActionsList\ActionsList;
use app\modules\donates\widgets\DonateBriefInfo;
use app\modules\donates\widgets\DonatePaymentsList;
use app\modules\donates\widgets\DonorNameAndContacts;
use app\modules\donates\widgets\PaymentBriefInfo;

?>

<?php foreach ($donates as $donate) : ?>
    <div class="card m-4">
        <div class="card-header">
            <?= DonateBriefInfo::widget([
                'donate' => $donate,
                'asLink' => true,
                'showStatus' => false
            ]) ?>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-sm-4">
                    <h6>Донор</h6>
                    <?= DonorNameAndContacts::widget([
                        'donor' => $donate->donor,
                        'multiLine' => false,
                        'asLink' => true
                    ]) ?>
                </div>
                <div class="col-sm-4">
                    <?php if ($donate->monthly) : ?>
                        <h6>Следующий платёж ожидается</h6>
                        <?php
                        $next = new DateTime($donate->nextPaymentAt);
                        $now = new DateTime();
                        if ($next < $now) {
                            $nextDateClass = 'text-warning';
                            $extraInfo = ' (просрочено)';
                        } else {
                            $nextDateClass = '';
                            $extraInfo = '';
                        }
                        ?>
                        <p class="<?= $nextDateClass ?>">
                            <?= Yii::$app->formatter->asDate($donate->nextPaymentAt) . $extraInfo ?>
                        </p>
                    <?php endif; ?>
                    <h6>Последние платежи</h6>
                    <?= DonatePaymentsList::widget([
                        'donate' => $donate,
                        'title' => '',
                        'renderMode' => DonatePaymentsList::LIST_MODE,
                        'limit' => 3,
                    ]) ?>
                </div>
                <div class="col-sm-4">
                    <?php
                    $actions = [
                        ['label' => 'История', 'icon' => 'fas fa-history', 'url' => ['monthly-history', 'id' => $donate->id]],
                    ];
                    if ($donate->enabled) {
                        $actions[] = [
                            'label' => 'Отключить',
                            'icon' => 'fas fa-times',
                            'url' => ['disable-donate', 'id' => $donate->id],
                            'options' => ['class' => 'text-danger'],
                        ];
                    } else {
                        $actions[] = [
                            'label' => 'Включить',
                            'icon' => 'fas fa-check',
                            'url' => [
                                'enable-donate',
                                'id' => $donate->id
                            ]
                        ];
                    }
                    if ($donate->isReadyForRepeat) {
                        $actions[] = [
                            'label' => 'Повторить',
                            'icon' => 'far fa-credit-card',
                            'url' => ['repeat-donate', 'id' => $donate->id]
                        ];
                    }
                    echo ActionsList::widget([
                        'items' => $actions,
                        'renderMode' => ActionsList::LIST_GROUP_MODE
                    ]) ?>
                </div>
            </div>
            <p class="card-text">

            </p>
        </div>
    </div>
<?php endforeach ?>