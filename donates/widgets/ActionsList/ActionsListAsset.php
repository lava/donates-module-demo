<?php

namespace app\modules\donates\widgets\ActionsList;

use yii\web\AssetBundle;

class ActionsListAsset extends AssetBundle
{
    public $sourcePath = __DIR__;
    public $css = [
        'actions-list-widget.css'
    ];
    public $js = [];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap4\BootstrapAsset',
        'app\assets\FontAwesomeAsset',
    ];

    public $publishOptions = [
        'forceCopy' => YII_DEBUG
    ];
}
