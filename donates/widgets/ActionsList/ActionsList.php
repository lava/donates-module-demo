<?php

namespace app\modules\donates\widgets\ActionsList;

use Closure;
use InvalidArgumentException;

use Yii;
use yii\bootstrap4\Widget;
use yii\bootstrap4\Html;




class ActionsList extends Widget
{
    const LIST_GROUP_MODE = 0;
    const LINKS_MODE = 1;
    const BUTTONS_MODE = 2;

    /**
     * Each item is array like ['label'=>..., 'url'=>..., 'icon'=..., 'options'=>...], 
     * 'icon' and 'options' are optional.
     *
     * @var array
     */
    public $items = [];

    /**
     * Rendering mode
     *
     * @var int
     */
    public $renderMode = self::LIST_GROUP_MODE;

    public function init()
    {
        parent::init();
        ActionsListAsset::register($this->view);
    }

    public function run()
    {
        if (empty($this->items)) return '';

        $html = Html::beginTag('div', ['class' => 'action-list-widget']);
        if ($this->renderMode == self::LIST_GROUP_MODE) {
            $html .= $this->renderAsListGroup();
        } else if ($this->renderMode == self::LINKS_MODE) {
            $html .= $this->renderAsLinks();
        } else if ($this->renderMode == self::BUTTONS_MODE) {
            $html .= $this->renderAsButtons();
        }
        $html .= Html::endTag('div');
        return $html;
    }

    protected function renderAsListGroup(): string
    {
        $html = Html::beginTag('div', ['class' => 'list-group']);
        foreach ($this->items as $action) {
            $icon = '';
            if (isset($action['icon'])) {
                $icon = $this->renderIcon($action['icon']) . ' ';
            }
            $options = ['class' => 'list-group-item list-group-item-action'];
            if (isset($action['options'])) {
                Html::addCssClass($options, $action['options']);
            }
            $html .= Html::a(
                "$icon$action[label]",
                $action['url'],
                $options
            );
        }
        $html .= Html::endTag('div');
        return $html;
    }

    protected function renderIcon(string $icon): string
    {
        return Html::tag('i', '', ['class' => $icon]);
    }

    protected function renderAsLinks(): string
    {
        $html = Html::beginTag('ul', ['class' => 'action-list-widget__links']);
        foreach ($this->items as $action) {
            $icon = '';
            if (isset($action['icon'])) {
                $icon = $this->renderIcon($action['icon']) . ' ';
            }
            $options = [];
            if (isset($action['options'])) {
                Html::addCssClass($options, $action['options']);
            }
            $html .= Html::beginTag('li', []);
            $html .= Html::a(
                "$icon$action[label]",
                $action['url'],
                $options
            );
            $html .= Html::endTag('li');
        }
        $html .= Html::endTag('ul');
        return $html;
    }

    protected function renderAsButtons(): string
    {
        $html = Html::beginTag('ul', ['class' => 'action-list-widget__buttons']);
        foreach ($this->items as $action) {
            $icon = '';
            if (isset($action['icon'])) {
                $icon = $this->renderIcon($action['icon']) . ' ';
            }
            $options = ['class' => 'btn btn-outline-primary'];
            if (isset($action['options'])) {
                Html::addCssClass($options, $action['options']);
            }
            $html .= Html::beginTag('li', []);
            $html .= Html::a(
                "$icon$action[label]",
                $action['url'],
                $options
            );
            $html .= Html::endTag('li');
        }
        $html .= Html::endTag('ul');
        return $html;
    }
}
