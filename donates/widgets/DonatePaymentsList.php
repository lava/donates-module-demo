<?php

namespace app\modules\donates\widgets;

use InvalidArgumentException;

use Yii;
use yii\bootstrap4\Widget;
use yii\bootstrap4\Html;

use app\modules\donates\components\DonateService;
use app\modules\donates\models\Donate;
use app\modules\donates\models\Payment;
use app\modules\donates\widgets\ActionsList\ActionsList;

class DonatePaymentsList extends Widget
{
    const TABLE_MODE = 0;
    const LIST_MODE = 1;

    /**
     * Donate or Donate's ID
     *
     * @var int|string|Donate
     */
    public $donate;

    /**
     * Maximum payments number to display
     *
     * @var integer
     */
    public $limit = 10;

    public $emptyDataMessage = 'Не найдено';

    public $title = 'Платежи по донату #{id}';
    public $titleOptions = [];

    public $renderMode = self::TABLE_MODE;


    /**
     * Items maybe Closure (like function(Payment $payment):string) or array like ['label'=>'','url'=>'']
     *
     * @var array
     */
    public $actions = [];

    /**
     * Payments
     *
     * @var Payment[]
     */
    protected $payments = [];

    protected $totalPaymentsCount = 0;



    public function run()
    {
        $this->loadPayments();
        if (empty($this->payments)) {
            return $this->emptyDataMessage;
        }

        if ($this->renderMode == self::TABLE_MODE) {
            return $this->renderAsTable();
        } else if ($this->renderMode == self::LIST_MODE) {
            return $this->renderAsList();
        }

        throw new InvalidArgumentException('Invalid render mode');
    }

    protected function renderAsList(): string
    {
        $html = $this->renderTitle();
        Html::addCssClass($this->options, ['class' => 'donate-payments-list-widget']);
        Html::addCssStyle($this->options, 'list-style-type:none;padding:0');
        $html .= Html::beginTag('ul', $this->options);
        foreach ($this->payments as $payment) {
            $html .= Html::beginTag('li');
            $html .= PaymentBriefInfo::widget([
                'payment' => $payment,
                'showErrorDescription' => false,
                'asLink' => true,
            ]);
            $html .= Html::endTag('li');
        }
        $html .= Html::endTag('ul');
        if ($this->limit > 0) {
            $html .= Html::tag('p', $this->renderItemsCount());
        }
        return $html;
    }

    protected function renderAsTable(): string
    {
        $html = $this->renderTitle();

        Html::addCssClass($this->options, ['class' => 'donate-payments-list-widget table table-striped ']);
        $html .= Html::beginTag('table', $this->options);

        $html .= Html::beginTag('thead', ['class' => 'thead-dark']);
        $html .= Html::beginTag('tr');
        $html .= Html::tag('th', 'ID');
        $html .= Html::tag('th', 'Тип');
        $html .= Html::tag('th', 'Дата');
        $html .= Html::tag('th', 'Сумма');
        $html .= Html::tag('th', 'Статус');
        $html .= Html::tag('th', '');
        $html .= Html::endTag('tr');
        $html .= Html::endTag('thead');

        if ($this->limit > 0) {
            $html .= Html::beginTag('tfoot');
            $html .= Html::beginTag('tr');
            $html .= Html::tag(
                'td',
                $this->renderItemsCount(),
                ['colspan' => 6]
            );
            $html .= Html::endTag('tr');
            $html .= Html::endTag('tfoot');
        }

        $html .= Html::beginTag('tbody');
        foreach ($this->payments as $payment) {
            $html .= Html::beginTag('tr');
            $html .= Html::tag('td', $payment->id);
            $html .= Html::tag('td', $this->renderType($payment));
            $html .= Html::tag('td', Yii::$app->formatter->asDatetime($payment->createdAt));
            $html .= Html::tag('td', Yii::$app->formatter->asCurrency($payment->amount));
            $html .= Html::tag('td', $this->renderStatus($payment));
            $html .= Html::tag('td', $this->renderActions($payment));
            $html .= Html::endTag('tr');
        }

        $html .= Html::endTag('tbody');

        $html .= Html::endTag('table');
        return $html;
    }




    protected function renderItemsCount(): string
    {
        $count = count($this->payments);
        return Html::tag('small', "Показано платежей: $count из $this->totalPaymentsCount");
    }

    protected function renderType(Payment $payment): string
    {
        return Html::tag('small', $payment->typeDesc);
    }

    protected function renderActions(Payment $payment): string
    {
        Yii::debug($payment);
        $actions = $this->actions;
        if (empty($actions)) {
            $actions = [
                ['label' => 'Подробнее', 'url' => ['payment-details', 'id' => $payment->id]]
            ];
        }
        return ActionsList::widget([
            'items' => $actions,
            'renderMode' => ActionsList::LINKS_MODE
        ]);
    }


    protected function renderStatus(Payment $payment): string
    {
        $status = '';
        $error = '';
        if ($payment->isSucceeded) {
            $status = 'Оплачено';
        } else if ($payment->isPending) {
            $status = 'Ожидание платежа';
        } else {
            $status = 'Платёж не прошёл';
            $error = Html::tag('small', DonateService::getInstance()->getPaymentErrorDescription($payment->error));
        }
        if (empty($error)) return $status;
        return $status . '<br>' . $error;
    }

    protected function renderTitle(): string
    {
        if (!$this->title) return '';
        $title = mb_ereg_replace('{id}', $this->donate->id, $this->title);
        return Html::tag('h5', $title, $this->titleOptions);
    }

    protected function loadPayments()
    {
        if (empty($this->donate)) {
            throw new InvalidArgumentException("Donate not set");
        }
        $id = 0;
        if (is_numeric($this->donate)) {
            $id = $this->donate;
        } else if ($this->donate instanceof Donate) {
            $id = $this->donate->id;
        }
        if (empty($id)) {
            throw new InvalidArgumentException("Invalid Donate type");
        }
        $query = Payment::find()
            ->where(['donateId' => $id])
            ->orderBy(['id' => SORT_DESC]);
        if ($this->limit > 0) {
            $query->limit = (int)$this->limit;
        }
        $this->totalPaymentsCount = $query->count();
        $this->payments = $query->all();
    }
}
