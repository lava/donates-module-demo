<?php

namespace app\modules\donates\widgets;

use Yii;
use yii\bootstrap4\Html;
use yii\bootstrap4\Widget;

use InvalidArgumentException;

use app\modules\donates\models\Donor;

class DonorNameAndContacts extends Widget
{
    /**
     * Donor's ID or Donor
     *
     * @var int|string|Donor
     */
    public $donor = null;
    public $multiLine = false;

    public $asLink = false;

    public $notFoundMessage = 'Не найдено';

    public function run()
    {
        $this->loadDonor();
        if (empty($this->donor)) {
            return $this->notFoundMessage;
        }

        Html::addCssClass($this->options, 'donor-name-widget');
        $html = Html::beginTag('div', $this->options);
        $info = $this->renderInfo();
        if ($this->asLink) {
            $html .= Html::a($info, ['donor-details', 'id' => $this->donor->id]);
        } else {
            $html .= $info;
        }
        $html .= Html::endTag('div');
        return $html;
    }

    protected function renderInfo(): string
    {
        $html = Html::tag(
            'span',
            '#' . $this->donor->id . ' ',
            ['class' => 'donor-name-widget__id']
        );
        if (!empty($this->donor->name)) {
            $html .= ' ';
            $html .= Html::tag(
                'span',
                Html::encode($this->donor->name),
                ['class' => 'donor-name-widget__name']
            );
            $html .= $this->renderSeparator();
        }
        if (!empty($this->donor->phone)) {
            if (empty($this->donor->name)) {
                $html .= $this->renderSeparator();
            }
            $html .= Html::tag(
                'span',
                Html::encode(Phonenumber::widget(['phone' => $this->donor->phone])),
                ['class' => 'donor-name-widget__phone']
            );
            $html .= $this->renderSeparator();
        }
        $html .= Html::tag(
            'span',
            Html::encode($this->donor->email),
            ['class' => 'donor-name-widget__email']
        );
        return $html;
    }

    protected function renderSeparator(): string
    {
        if ($this->multiLine) {
            return '<br/>';
        } else {
            return ' ';
        }
    }



    protected function loadDonor()
    {
        if (empty($this->donor)) {
            throw new InvalidArgumentException("Donor not set");
        }
        if (is_numeric($this->donor)) {
            $this->donor = Donor::findOne($this->donor);
        }

        if (!($this->donor instanceof Donor)) {
            throw new InvalidArgumentException("Invalid Donor");
        }
    }
}
