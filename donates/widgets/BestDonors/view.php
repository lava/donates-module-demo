<?php

/** @var yii\web\View $this */
/** @var app\modules\donates\models\Donor[] $donors */

use app\modules\donates\widgets\DonorNameAndContacts;

?>
<table class="table table-sm table-bordered best-donors-widget">
    <thead class="thead-dark">
        <tr>
            <!-- <th>id</th> -->
            <th>Донор</th>
            <th>Кол-во донатов</th>
            <th>Общая сумма платежей</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($donors as $donor) : ?>
            <?php $name = '';
            ?>
            <tr>
                <!-- <td class="best-donors-widget__id">
                    <?= $donor->id ?>
                </td> -->
                <td class="best-donors-widget__payments-amount">
                    <?= DonorNameAndContacts::widget(['donor' => $donor, 'multiLine' => true]) ?>
                </td>
                <td class="best-donors-widget__donates-count">
                    <?= $donor->totalDonatesCount ?>
                </td>
                <td class="best-donors-widget__payments-amount">
                    <?= Yii::$app->formatter->asCurrency($donor->totalPaymentsAmount) ?>
                </td>
            </tr>
        <?php endforeach ?>
    </tbody>
</table>