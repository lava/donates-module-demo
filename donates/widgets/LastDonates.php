<?php

namespace app\modules\donates\widgets;

use yii\bootstrap4\Widget;
use app\modules\donates\models\Donate;

/**
 * Undocumented class
 * 
 * @depends DonorNameAndContacts widget
 */
class LastDonates extends Widget
{
    /** 
     * Items to display.
     * 
     * @var \yii\data\ActiveDataProvider 
     */
    public $dataProvider = null;

    /**
     * Items to display. Used if $this->dataProvider is empty.
     * If $this->donates not set, it will be autoloaded.
     *
     * @var Donate[]
     */
    public $items = [];

    public $limit = 15;


    public function run()
    {
        if (!empty($this->dataProvider)) {
            $this->items = $this->dataProvider->limit($this->limit)->all();
        } else if (empty($this->items)) {
            $this->items = Donate::find()
                ->with(['donor'])
                ->orderBy(['id' => SORT_DESC])
                ->limit($this->limit)
                ->all();
        }

        return $this->renderFile(
            __DIR__ . DIRECTORY_SEPARATOR . 'LastDonates' . DIRECTORY_SEPARATOR . 'view.php',
            [
                'donates' => $this->items,
            ]
        );
    }
}
