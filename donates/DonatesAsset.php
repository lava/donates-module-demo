<?php

namespace app\modules\donates;

use yii\web\AssetBundle;

class DonatesAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [];
    public $js = [];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap4\BootstrapAsset',
        'app\assets\FontawesomeAsset',
    ];
}
