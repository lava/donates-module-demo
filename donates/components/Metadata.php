<?php

namespace app\modules\donates\components;

use app\modules\donates\components\PaymentService\Response;
use app\modules\donates\models\Payment;


/**
 * Undocumented class
 * 
 * @property int $donateId
 * @property int $paymentId
 * @property string $notificationSourceVerificationToken
 */
class Metadata extends \yii\base\BaseObject
{
    const DONATE_ID_NAME = 'donate_id';
    const PAYMENT_ID_NAME = 'payment_id';
    const TOKEN_NAME = 'nsvt';

    private $donateIdValue = 0;
    private $paymentIdValue = 0;
    private $notificationSourceVerificationToken = '';

    public static function createFromArray(array $data): Metadata
    {
        $item = new Metadata();
        if (isset($data[self::PAYMENT_ID_NAME])) {
            $item->paymentIdValue = (int)$data[self::PAYMENT_ID_NAME];
        }
        if (isset($data[self::DONATE_ID_NAME])) {
            $item->donateIdValue = (int)$data[self::DONATE_ID_NAME];
        }
        if (isset($data[self::TOKEN_NAME])) {
            $item->notificationSourceVerificationToken = $data[self::TOKEN_NAME];
        }
        return $item;
    }

    public static function createFromResponse(Response $info): Metadata
    {
        return self::createFromArray($info->metadata);
    }

    public static function createFromPayment(Payment $payment): Metadata
    {
        return self::createFromArray([
            self::DONATE_ID_NAME => $payment->donateId,
            self::PAYMENT_ID_NAME => $payment->id,
            self::TOKEN_NAME => self::generateNotificationSourceVerificationToken($payment)
        ]);
    }

    public function getPaymentId(): int
    {
        return $this->paymentIdValue;
    }

    public function getDonateId(): int
    {
        return $this->donateIdValue;
    }

    public function getNotificationSourceVerificationToken(): string
    {
        return $this->notificationSourceVerificationToken;
    }

    public function toArray(): array
    {
        return [
            self::DONATE_ID_NAME => $this->donateIdValue,
            self::PAYMENT_ID_NAME => $this->paymentIdValue,
            self::TOKEN_NAME => $this->notificationSourceVerificationToken,
        ];
    }

    private static function generateNotificationSourceVerificationToken(Payment $payment): string
    {
        return md5(self::TOKEN_NAME . $payment->id);
    }
}
