<?php

namespace app\modules\donates\components;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;

class BaseController extends Controller
{
    public $layout = 'main';

    protected $generalViewParams = [];

    protected $userCanRead = true;
    protected $userCanWrite = false;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        // $rules = [];
        // if (!empty($this->module->rbacPermissionForAccess)) {
        //     $rules[] = [
        //         'allow' => true,
        //         'roles' => [$this->module->rbacPermissionForAccess],
        //     ];
        // }
        // $rules[] = [
        //     'allow' => false,
        //     'roles' => ['?'],
        // ];

        // return [
        //     'access' => [
        //         'class' => AccessControl::class,
        //         'rules' => [ //$rules
        //             [
        //                 'allow' => false,
        //                 'roles' => ['?'],
        //             ]
        //         ]
        //     ],
        // ];
        return [];
    }

    public function init()
    {
        $this->userCanRead = true;
        if (!empty($this->module->rbacPermissionForRead) && !Yii::$app->user->can($this->module->rbacPermissionForRead)) {
            $this->userCanRead = false;
        }
        $this->userCanWrite = false;
        if (!empty($this->module->rbacPermissionForWrite) && Yii::$app->user->can($this->module->rbacPermissionForWrite)) {
            $this->userCanWrite = true;
        }
        $this->generalViewParams[] = [
            'userCanRead' => $this->userCanRead,
            'userCanWrite' => $this->userCanWrite,
        ];
        return parent::init();
    }

    protected function prepareViewParams(array $params = []): array
    {
        return array_merge($params, $this->generalViewParams);
    }
}
