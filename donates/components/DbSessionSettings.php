<?php

namespace app\modules\donates\components;

use Yii;

class DbSessionSettings
{
    private static $previousSessionWaitTimeout = 30;

    public static function getWaitTimeout()
    {
        $result = Yii::$app->db->createCommand('SHOW SESSION VARIABLES LIKE "wait_timeout"')->queryOne();
        if ($result && isset($result['Value'])) {
            return $result['Value'];
        }
        return false;
    }

    public static function setWaitTimeout(int $time)
    {
        $v = self::getWaitTimeout();
        if ($v !== false) self::$previousSessionWaitTimeout = $v;
        Yii::$app->db->createCommand('SET session wait_timeout=:t', [':t' => $time])->execute();
    }

    public  static function restoreWaitTimeout()
    {
        Yii::$app->db->createCommand('SET session wait_timeout=:t', [':t' => intval(self::$previousSessionWaitTimeout)])->execute();
    }
}
