<?php

namespace app\modules\donates\components\PaymentService;

use app\modules\donates\components\Metadata;
use app\modules\donates\models\Payment;

class Request //extends \yii\base\BaseObject
{
    /**
     * Payment's amount
     *
     * @var integer
     */
    public $amountValue = 0;

    /**
     * Payment's amount currency
     *
     * @var integer
     */
    public $amountCurrency = 'RUB';

    /**
     * Payment's metadata
     *
     * @var array
     */
    public $metadata = [];

    public $description = '';

    public $isAutoAccepted = true;

    public $isRecurrent = false;

    public $recurrentPaymentId = '';


    public static function createFromPayment(Payment $payment): Request
    {
        $item = new Request();
        $item->amountValue = $payment->amount;
        $item->metadata = Metadata::createFromPayment($payment)->toArray();
        $item->description = PaymentDescription::createFromPayment($payment);
        $item->isRecurrent = $payment->isMonthly;
        $item->recurrentPaymentId = $payment->recurrentPaymentOperationId ?? '';
        return $item;
    }

    public function toArray(): array
    {
        return [
            'amountValue' => $this->amountValue,
            'amountCurrency' => $this->amountCurrency,
            'metadata' => $this->metadata,
            'description' => $this->description,
            'isAutoAccepted' => $this->isAutoAccepted,
            'isRecurrent' => $this->isRecurrent,
            'recurrentPaymentId' => $this->recurrentPaymentId,
        ];
    }
}
