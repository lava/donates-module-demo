<?php

namespace app\modules\donates\components\PaymentService;

use app\modules\donates\models\Payment;

class PaymentDescription //extends \yii\base\BaseObject
{
    const SINGLE_PAYMENT_DESCRIPTION = 'Пожертвование для приюта АНО ЦРА';
    const MONTHLY_PAYMENT_DESCRIPTION = 'Ежемесячное пожертвование для приюта АНО ЦРА';

    public $text = '';

    public static function createFromPayment(Payment $payment)
    {
        $item = new PaymentDescription();
        if ($payment->isSingle) {
            $item->text = self::SINGLE_PAYMENT_DESCRIPTION;
        } else if ($payment->isMonthly) {
            $item->text = self::MONTHLY_PAYMENT_DESCRIPTION;
        }
    }

    public function __toString()
    {
        return $this->text;
    }
}
