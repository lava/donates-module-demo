<?php

namespace app\modules\donates\components\PaymentService;

/**
 * 
 * @property bool $isSucceeded
 * @property bool $isPending
 * @property bool $isCanceled
 */
class Response extends \yii\base\BaseObject
{
    const STATUS_UNKNOWN = 0;
    const STATUS_PENDING = 1;
    const STATUS_SUCCEEDED = 2;
    const STATUS_CANCELED = 3;

    /**
     * Payment's ID (from payment gateway)
     *
     * @var string
     */
    public $paymentId = '';

    /**
     * Recurrent payment's ID (from payment gateway)
     *
     * @var string
     */
    public $recurrentPaymentId = '';

    /**
     * Status of payment
     *
     * @var integer
     */
    public $status = 0;

    /**
     * Cancellation resone code
     *
     * @var string
     */
    public $cancellationReason = '';

    /**
     * Description of the cancellation reason
     *
     * @var string
     */
    public $cancellationReasonDescription = '';

    /**
     * Payment's amount
     *
     * @var integer
     */
    public $amountValue = 0;

    /**
     * Payment's amount currency
     *
     * @var integer
     */
    public $amountCurrency = 'RUB';

    /**
     * Payment's datetime
     *
     * @var \DateTime
     */
    public $date = null;

    /**
     * Payment's metadata
     *
     * @var array
     */
    public $metadata = [];

    /**
     * Response of the payment service gateway
     *
     * @var array
     */
    public $apiResponse = [];

    /**
     * Request which response to
     *
     * @var array
     */
    public $apiRequest = [];


    public function getIsSucceeded(): bool
    {
        return $this->status == self::STATUS_SUCCEEDED;
    }

    public function getIsPending(): bool
    {
        return $this->status == self::STATUS_PENDING;
    }

    public function getIsCanceled(): bool
    {
        return $this->status == self::STATUS_CANCELED;
    }

    public function toArray(): array
    {
        return [
            'paymentId' => $this->paymentId,
            'recurrentPaymentId' => $this->recurrentPaymentId,
            'status' => $this->status,
            'cancellationReason' => $this->cancellationReason,
            'cancellationReasonDescription' => $this->cancellationReasonDescription,
            'amountValue' => $this->amountValue,
            'amountCurrency' => $this->amountCurrency,
            'date' => $this->date,
            'metadata' => $this->metadata,
            'apiResponse' => $this->apiResponse,
            'apiRequest' => $this->apiRequest,
        ];
    }
}
