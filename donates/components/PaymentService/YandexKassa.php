<?php

namespace app\modules\donates\components\PaymentService;

use Yii;

use YandexCheckout\Client;
use YandexCheckout\Model\CancellationDetailsReasonCode;
use YandexCheckout\Model\MonetaryAmount;
use YandexCheckout\Request\Payments\CreatePaymentRequest;
use YandexCheckout\Model\ConfirmationAttributes\ConfirmationAttributesEmbedded;
use YandexCheckout\Model\PaymentInterface;
use YandexCheckout\Model\Notification\NotificationFactory;

/**
 * Workinkg with Yandex.Kassa (YooMoney).
 * 
 * e.g.:
 *      $service = new YandexKassa(['shopId' => '...', 'apiKey' => '...']);
 *      $response = $service->makePayment($request);
 * 
 * @property callable|LoggerInterface $logger Optional. Use standard logger description: function($level, $message, $context)
 * @property PaymentInterface|null $lastApiResponse
 */
class YandexKassa extends \yii\base\Component implements IPaymentService
{
    /**
     * YandexKassa shop identificator
     *
     * @var string
     */
    public $shopId = '';

    /**
     * YandexKassa API-access token
     *
     * @var string
     */
    public $apiKey = '';


    /**
     * 
     * @var \YandexCheckout\Client
     */
    private $kassa = null;

    /**
     *
     * @var callable|LoggerInterface
     */
    private $logMethod = null;

    public function getLogger(): callable
    {
        return $this->logMethod;
    }

    public function setLogger(callable $logger)
    {
        $this->logMethod = $logger;
        $this->kassa->setLogger($logger);
    }

    public function init()
    {
        $this->kassa = new Client();
        if (empty($this->shopId)) {
            $this->shopId = $this->getShopIdFromConfig();
        }
        if (empty($this->apiKey)) {
            $this->apiKey = $this->getApiKeyFromConfig();
        }
        $this->kassa->setAuth($this->shopId, $this->apiKey);
    }

    private function getShopIdFromConfig(): string
    {
        $name = YII_ENV_PROD ? 'kassa.real.shopId' : 'kassa.test.shopId';
        return Yii::$app->params[$name] ?? '';
    }

    private function getApiKeyFromConfig(): string
    {
        $name = YII_ENV_PROD ? 'kassa.real.apiKey' : 'kassa.test.apiKey';
        return Yii::$app->params[$name] ?? '';
    }

    public function getServiceParams(): array
    {
        return [
            'shopId' => $this->shopId,
            'apiKey' => mb_substr($this->apiKey, 0, 5) . '***',
        ];
    }

    function setServiceParams(array $params)
    {
        //not imlpemented
    }


    /**
     * Makes single payment
     *
     * @param Request $request
     * @return Response
     */
    public function makeSinglePayment(Request $request): Response
    {
        $kassaRequest = $this->convertRequestToKassaPaymentRequest($request);
        $kassaRequest->save_payment_method = false;
        $kassaRequest->setConfirmation(new ConfirmationAttributesEmbedded());
        return $this->makePayment($kassaRequest);
    }


    /**
     * Makes recurring payment first time
     *
     * @param Request $request
     * @return Response
     */
    public function makeFirstRecurringPayment(Request $request): Response
    {
        $kassaRequest = $this->convertRequestToKassaPaymentRequest($request);
        $kassaRequest->save_payment_method = true;
        $kassaRequest->setConfirmation(new ConfirmationAttributesEmbedded());
        return $this->makePayment($kassaRequest);
    }


    /**
     * Repeats saved recurring payment
     *
     * @param Request $request
     * @return Response
     */
    public function makeNextRecurringPayment(Request $request): Response
    {
        if (empty($request->recurrentPaymentId)) {
            Yii::warning('Request has empty recurrentPaymentId');
        }
        $kassaRequest = $this->convertRequestToKassaPaymentRequest($request);
        $kassaRequest->paymentMethodId = $request->recurrentPaymentId;
        return $this->makePayment($kassaRequest);
    }

    private function makePayment(CreatePaymentRequest $kassaRequest): Response
    {
        $kassaResponse = $this->kassa->createPayment($kassaRequest);
        $response = $this->convertKassaPaymentResponseToResponse($kassaResponse);
        $response->apiRequest = $kassaRequest->jsonSerialize();
        // Yii::debug($response->apiRequest);
        return $response;
    }



    private function convertRequestToKassaPaymentRequest(Request $request): CreatePaymentRequest
    {
        $kassaRequest = new CreatePaymentRequest();
        $kassaRequest->capture = $request->isAutoAccepted;
        $kassaRequest->amount = new MonetaryAmount($request->amountValue, $request->amountCurrency);
        $kassaRequest->description = $request->description;
        $kassaRequest->metadata = $request->metadata;
        return $kassaRequest;
    }

    private function convertKassaPaymentResponseToResponse(PaymentInterface $kassaResponse): Response
    {
        $response = new Response();
        $response->paymentId = $kassaResponse->id;
        $this->setResponseRecurrentPaymentIdByKassaResponse($response, $kassaResponse);
        $this->setResponseStatusByKassaResponse($response, $kassaResponse);
        $this->setResponseCancelationByKassaResponse($response, $kassaResponse);
        $response->amountValue = $kassaResponse->amount->value;
        $response->amountCurrency = $kassaResponse->amount->currency;
        $response->date = $kassaResponse->createdAt;
        $response->metadata = $kassaResponse->metadata->toArray();
        $response->apiResponse = $kassaResponse->jsonSerialize();
        return $response;
    }


    /**
     * Returns human readable error description
     *
     * @param string $errorCode
     * @return string
     */
    public function getPaymentErrorDescription(string $errorCode): string
    {
        return $this->getCancellationReasonDescription($errorCode);
    }

    /**
     * Returns information about existing payment
     *
     * @param string $paymentId
     * @return Response
     */
    public function queryPaymentStatus(string $paymentId): Response
    {
        $kassaResponse = $this->kassa->getPaymentInfo($paymentId);
        $response = $this->convertKassaPaymentResponseToResponse($kassaResponse);
        $response->apiRequest = null;
        return $response;
    }

    private function setResponseRecurrentPaymentIdByKassaResponse(Response &$response, PaymentInterface $kassaResponse): Response
    {
        if ($kassaResponse->paymentMethod) {
            if ($kassaResponse->paymentMethod->saved) {
                $response->recurrentPaymentId = $kassaResponse->paymentMethod->id;
            }
        }
        return $response;
    }

    private function setResponseCancelationByKassaResponse(Response &$response, PaymentInterface $kassaResponse): Response
    {
        if ($response->isCanceled) {
            $response->cancellationReason = $kassaResponse->getCancellationDetails()->reason;
            $response->cancellationReasonDescription = $this->getCancellationReasonDescription($response->cancellationReason);
        }
        return $response;
    }

    /**
     * Converts payment's information from Kassa to Response object
     *
     * @param array $data Raw data of notification response from Kassa
     * @return Response
     */
    public function convertPaymentNotificationToResponse(array $data): Response
    {
        $factory = new NotificationFactory();
        $notification = $factory->factory($data);
        /** @var PaymentInterface $kassaResponse */
        $kassaResponse = $notification->getObject();
        $response = new Response();
        $response->paymentId = $kassaResponse->id;
        $response->date = $kassaResponse->createdAt;
        $this->setResponseRecurrentPaymentIdByKassaResponse($response, $kassaResponse);
        $this->setResponseStatusByKassaResponse($response, $kassaResponse);
        $this->setResponseCancelationByKassaResponse($response, $kassaResponse);
        $response->amountValue = $kassaResponse->amount->value;
        $response->amountCurrency = $kassaResponse->amount->currency;
        $response->metadata = $kassaResponse->metadata->toArray();
        $response->apiResponse = $data;
        return $response;
    }


    private function setResponseStatusByKassaResponse(Response &$response, PaymentInterface $kassaResponse): Response
    {
        $statuses = [
            'pending' => Response::STATUS_PENDING,
            'succeeded' => Response::STATUS_SUCCEEDED,
            'waiting_for_capture' => Response::STATUS_PENDING,
            'canceled' => Response::STATUS_CANCELED,
        ];
        if (isset($statuses[$kassaResponse->status])) {
            $status = $statuses[$kassaResponse->status];
        } else {
            $status = Response::STATUS_UNKNOWN;
        }
        $response->status = $status;
        return $response;
    }



    private function getCancellationReasonDescription(string $cancellationReason): string
    {
        switch ($cancellationReason) {
            case CancellationDetailsReasonCode::THREE_D_SECURE_FAILED:
                return 'Не пройдена проверка по 3-D Secure банка. Попробуйте повторить платёж или обратитесь в ваш банк.';
            case CancellationDetailsReasonCode::CALL_ISSUER:
                return 'Ваш банк отклонил платёж по неизвестной причине.';
            case CancellationDetailsReasonCode::CARD_EXPIRED:
                return 'Истек срок действия банковской карты.';
            case CancellationDetailsReasonCode::COUNTRY_FORBIDDEN:
                return 'Нельзя заплатить банковской картой, выпущенной в этой стране.';
            case CancellationDetailsReasonCode::FRAUD_SUSPECTED:
                return 'Платеж заблокирован из-за подозрения в мошенничестве. ';
            case CancellationDetailsReasonCode::IDENTIFICATION_REQUIRED:
                return 'Превышены ограничения на платежи для кошелька в Яндекс.Деньгах.';
            case CancellationDetailsReasonCode::INSUFFICIENT_FUNDS:
                return 'Не хватает денег для оплаты.';
            case CancellationDetailsReasonCode::INVALID_CARD_NUMBER:
                return 'Неправильно указан номер карты.';
            case CancellationDetailsReasonCode::INVALID_CSC:
                return 'Неправильно указан код CVV2 (CVC2, CID).';
            case CancellationDetailsReasonCode::ISSUER_UNAVAILABLE:
                return 'Организация, выпустившая платежное средство, недоступна.';
            case CancellationDetailsReasonCode::PAYMENT_METHOD_LIMIT_EXCEEDED:
                return 'Исчерпан лимит платежей.';
            case CancellationDetailsReasonCode::PAYMENT_METHOD_RESTRICTED:
                return 'Запрещены операции данным платежным средством (например, карта заблокирована из-за утери, кошелек — из-за взлома мошенниками)';
            case CancellationDetailsReasonCode::PERMISSION_REVOKED:
                return 'Нельзя провести безакцептное списание.';
            case CancellationDetailsReasonCode::EXPIRED_ON_CONFIRMATION:
                return 'Истек срок оплаты.';
            case CancellationDetailsReasonCode::INTERNAL_TIMEOUT:
                return 'Технические неполадки на стороне ЮKassa.'; //FIXME hardcode
            case 'unsupported_mobile_operator':
                return 'Нельзя заплатить с номера телефона этого мобильного оператора.';
            case CancellationDetailsReasonCode::GENERAL_DECLINE:
                return 'Неизвестная ошибка оператора платёжного средства (банка).';
            default:
                Yii::warning("Failed to detect cancellationReason: '$cancellationReason'");
                return 'Неизвестная ошибка.';
        }
    }
}
