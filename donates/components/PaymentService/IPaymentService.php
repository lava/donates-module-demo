<?php

namespace app\modules\donates\components\PaymentService;

use app\modules\donates\models\Donate;

interface IPaymentService
{
    /**
     * Makes single payment
     *
     * @param Request $request
     * @return Response
     */
    function makeSinglePayment(Request $request): Response;

    /**
     * Makes first monthly payment
     *
     * @param Request $request
     * @return Response
     */
    function makeFirstRecurringPayment(Request $request): Response;

    /**
     * Makes repeat for monthly payment
     *
     * @param Request $request
     * @return Response
     */
    function makeNextRecurringPayment(Request $request): Response;

    /**
     * Gets payment info
     *
     * @param string $paymentId
     * @return Response
     */
    function queryPaymentStatus(string $paymentId): Response;


    /**
     * Converts payment's notification from payment service to Response object
     *
     * @param array $data
     * @return Response
     */
    function convertPaymentNotificationToResponse(array $data): Response;

    /**
     * Returns human readable description for the error.
     *
     * @param string $errorCode
     * @return string
     */
    function getPaymentErrorDescription(string $errorCode): string;

    /**
     * Gets service-specific params
     *
     * @return array
     */
    function getServiceParams(): array;

    /**
     * Sets service-specific params
     *
     * @param array $params
     * @return void
     */
    function setServiceParams(array $params);
}
