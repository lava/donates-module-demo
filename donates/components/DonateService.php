<?php

namespace app\modules\donates\components;

use Yii;
use yii\helpers\VarDumper;
use Exception;
use InvalidArgumentException;

use app\modules\donates\components\NotificationService\EmailService;
use app\modules\donates\components\NotificationService\INotificationService;
use app\modules\donates\components\PaymentService\IPaymentService;
use app\modules\donates\components\PaymentService\Request;
use app\modules\donates\components\PaymentService\Response;
use app\modules\donates\components\PaymentService\YandexKassa;

use app\modules\donates\models\Donate;
use app\modules\donates\models\DonateCategory;
use app\modules\donates\models\Donor;
use app\modules\donates\models\Payment;
use app\modules\donates\models\PaymentServiceOperation;
use yii\helpers\Json;

/**
 * Undocumented class
 * 
 * @property IPaymentService $paymentService
 * @property INotificationService $notificationService
 * @property Response $lastPaymentServiceResponse
 */
class DonateService extends \yii\base\Component
{
    // public $sendNotifications = true;

    /**
     *
     * @var IPaymentService
     */
    private $paymentServiceInstance = null;

    /**
     *
     * @var INotificationService
     */
    private $notificationServiceInstance = null;


    public function getPaymentService(): IPaymentService
    {
        if (!$this->paymentServiceInstance) {
            $this->paymentServiceInstance = new YandexKassa();
        }
        return $this->paymentServiceInstance;
    }

    public function setPaymentService(IPaymentService $service)
    {
        $this->paymentServiceInstance = $service;
    }

    public function getNotificationService(): INotificationService
    {
        if (!$this->notificationServiceInstance) {
            $this->notificationServiceInstance = new EmailService();
            $this->notificationServiceInstance->setSenderEmail(Yii::$app->params['senderEmail']);
        }
        return $this->notificationServiceInstance;
    }

    public function setNotificationService(INotificationService $service)
    {
        $this->notificationServiceInstance = $service;
    }

    private static $donateServiceInstance = null;
    public static function getInstance(array $config = []): self
    {
        if (empty(self::$donateServiceInstance)) {
            self::$donateServiceInstance = new DonateService($config);
        }
        return self::$donateServiceInstance;
    }

    /**
     *
     * @var Response
     */
    private $lastResponse = null;

    public function getLastPaymentServiceResponse(): ?Response
    {
        return $this->lastResponse;
    }

    private function setLastPaymentServiceResponse(Response $response)
    {
        $this->lastResponse = $response;
    }

    private function clearLastPaymentServiceResponse()
    {
        $this->lastResponse = null;
    }

    /**
     * Undocumented function
     *
     * @param array $attrs
     * @return Donor|null
     */
    public function createDonor(array $attrs): ?Donor
    {
        $item = new Donor($attrs);
        if (!$item->save()) {
            Yii::error('Failed to save Donor');
            $this->dumpModel($item);
            return null;
        }
        return $item;
    }


    /**
     * Undocumented function
     *
     * @param array $attrs
     * @return Donor|null
     */
    public function createOrUpdateDonor(array $attrs): ?Donor
    {
        $item = $this->findDonor($attrs);
        if ($item) {
            $item->setAttributes($attrs);
        } else {
            $item = new Donor($attrs);
        }
        if (!$item->save()) {
            Yii::error('Failed to save Donor');
            $this->dumpModel($item);
            return null;
        }
        return $item;
    }


    /**
     * Undocumented function
     *
     * @param array $attrs Attributes for Donate (can include 'donor' item with attributes for Donor)
     * @return Donate|null
     */
    public function createSingleDonate(array $attrs): ?Donate
    {
        $attrs['monthly'] = false;
        $donate = $this->createDonate($attrs);

        return $donate;
    }


    /**
     * Undocumented function
     *
     * @param array $attrs Attributes for Donate (can include 'donor' item with attributes for Donor)
     * @return Donate|null
     */
    public function createMonthlyDonate(array $attrs): ?Donate
    {
        $attrs['monthly'] = true;
        return $this->createDonate($attrs);
    }


    /**
     * Makes next monthly donate
     *
     * @param Donate $donate
     * @return Payment|null
     */
    public function repeatPayment(Donate $donate): ?Payment
    {
        Yii::debug($donate->attributes);
        $transaction = Yii::$app->db->beginTransaction();
        try {
            if (!$donate->monthly) {
                throw new Exception("Can't repeat single donate");
            }
            $payment = new Payment();
            $payment->donateId = $donate->id;
            $payment->type = Payment::TYPE_MONTHLY_NEXT;
            $payment->status = Payment::STATUS_PENDING;
            $payment->amount = $donate->amount;
            if (!$payment->save()) {
                $this->dumpModel($payment);
                throw new Exception('Failed to save Payment');
            }

            $request = Request::createFromPayment($payment);
            $this->clearLastPaymentServiceResponse();
            $response = $this->paymentService->makeNextRecurringPayment($request);
            $this->setLastPaymentServiceResponse($response);
            $this->setPaymentStatusByResponse($payment, $response);
            if (!$payment->save()) {
                $this->dumpModel($payment);
                throw new Exception('Failed to update Payment');
            }

            $operation = $this->createOrUpdatePaymentServiceOperation(
                $payment,
                $response->paymentId,
                [
                    'request' => $response->apiRequest,
                    'response' => $response->apiResponse,
                    'recurrentOperationId' => $response->recurrentPaymentId,
                ]
            );
            if (!$operation) {
                throw new Exception('Failed to create PaymentServiceOperation');
            }

            $donate = $this->updateDonateAfterPayment($donate, $payment);

            $transaction->commit();
            return $payment;
        } catch (\Throwable $e) {
            $transaction->rollBack();
            $this->dumpException($e);
            return null;
        }
    }


    /**
     * Makes single or first of monthly donate payment
     *
     * @param Donate $donate
     * @return Payment|null
     */
    public function makePayment(Donate $donate): ?Payment
    {
        Yii::debug($donate->attributes);
        $transaction = Yii::$app->db->beginTransaction();
        try {
            $payment = $this->createPayment($donate);
            if (!$payment) {
                throw new Exception('Failed to create Payment');
            }

            $request = Request::createFromPayment($payment);
            $this->clearLastPaymentServiceResponse();
            if ($payment->isSingle) {
                $response = $this->paymentService->makeSinglePayment($request);
            } else if ($payment->isMonthlyFirst) {
                $response = $this->paymentService->makeFirstRecurringPayment($request);
            } else {
                throw new Exception('Uknown payment type');
            }
            $this->setLastPaymentServiceResponse($response);
            $this->setPaymentStatusByResponse($payment, $response);
            if (!$payment->save()) {
                $this->dumpModel($payment);
                throw new Exception('Failed to update Payment');
            }

            $operation = $this->createOrUpdatePaymentServiceOperation(
                $payment,
                $response->paymentId,
                [
                    'request' => $response->apiRequest,
                    'response' => $response->apiResponse,
                    'recurrentOperationId' => $response->recurrentPaymentId,
                ]
            );
            if (!$operation) {
                throw new Exception('Failed to create PaymentServiceOperation');
            }

            $donate = $this->updateDonateAfterPayment($donate, $payment);

            $transaction->commit();
            return $payment;
        } catch (\Throwable $e) {
            $transaction->rollBack();
            $this->dumpException($e);
            return null;
        }
    }

    private function updateDonateAfterPayment(Donate $donate, Payment $payment): Donate
    {
        if ($payment->isSucceeded) {
            $donate->lastPaidAt = $payment->createdAt;
            $donate->calculateNextPaymentDateForNextMonth();
        } else if ($payment->isCanceled) {
            $donate->addDaysToNextPaymentDate(1);
        }
        if (!$donate->save()) {
            Yii::error('Failed to save Donate');
            $this->dumpModel($donate);
        }
        return $donate;
    }


    /**
     * Undocumented function
     *
     * @param int|string|Donate $donate Donate or it's id (int value) or hash (string value).
     * @return boolean
     */
    public function disableDonate($donate): bool
    {
        if (is_int($donate)) {
            $donate = Donate::findOne($donate);
        } else if (is_string($donate)) {
            $donate = Donate::findByHash($donate);
        }
        if (!$donate || !($donate instanceof Donate)) {
            Yii::error('Donate not found');
            return false;
        }
        $donate->enabled = false;
        if (!$donate->save()) {
            Yii::error('Failed to save Donate');
            $this->dumpModel($donate);
            return false;
        }
        return true;
    }

    public function enableDonate($donate)
    {
        if (is_int($donate)) {
            $donate = Donate::findOne($donate);
        } else if (is_string($donate)) {
            $donate = Donate::findByHash($donate);
        }
        if (!$donate || !($donate instanceof Donate)) {
            Yii::error('Donate not found');
            return false;
        }
        $donate->enabled = true;
        if (!$donate->save()) {
            Yii::error('Failed to save Donate');
            $this->dumpModel($donate);
            return false;
        }
        return true;
    }


    private function setPaymentStatusByResponse(Payment &$payment, Response $response): Payment
    {
        if ($response->isSucceeded) {
            $payment->status = Payment::STATUS_SUCCEEDED;
        } else if ($response->isCanceled) {
            $payment->status = Payment::STATUS_CANCELED;
            $payment->error = $response->cancellationReason;
        } else if ($response->isPending) {
            $payment->status = Payment::STATUS_PENDING;
        }
        return $payment;
    }

    public function getPaymentErrorDescription(string $error): string
    {
        return $this->paymentService->getPaymentErrorDescription($error);
    }

    /**
     * Undocumented function
     *
     * @param array|Response $data Payment service notification data or Response object
     * @return Payment|null
     */
    public function updatePaymentStatus($data): ?Payment
    {
        $transaction = Yii::$app->db->beginTransaction();
        try {
            if (is_array($data)) {
                $payment = $this->updatePaymentStatusByNotification($data);
            } else if ($data instanceof Response) {
                $payment = $this->updatePaymentStatusByResponse($data);
            } else {
                throw new InvalidArgumentException('Invalid data');
            }
            $transaction->commit();
            return $payment;
        } catch (\Throwable $e) {
            $transaction->rollBack();
            $this->dumpException($e);
            return null;
        }
    }

    private function updatePaymentStatusByResponse(Response $response): ?Payment
    {
        $meta = Metadata::createFromResponse($response);
        $payment = Payment::findOne($meta->paymentId);
        if (!$payment) {
            throw new Exception("Payment id=$meta->paymentId not found");
        }
        $this->setPaymentStatusByResponse($payment, $response);
        if (!$payment->save()) {
            $this->dumpModel($payment);
            throw new Exception('Failed to update Payment');
        }
        $operation = $this->createOrUpdatePaymentServiceOperation(
            $payment,
            $response->paymentId,
            [
                'request' => empty($response->apiRequest) ? null : $response->apiRequest,
                'response' => $response->apiResponse,
                'recurrentOperationId' => $response->recurrentPaymentId,
            ]
        );
        if (!$operation) {
            throw new Exception('Failed to create PaymentServiceOperation');
        }
        // PaymentServiceOperation::createOrUpdate($response);
        return $payment;
    }


    private function updatePaymentStatusByNotification(array $data): ?Payment
    {
        $info = $this->paymentService->convertPaymentNotificationToResponse($data);
        return $this->updatePaymentStatusByResponse($info);
    }



    private function findDonor(array $donorAttrs): ?Donor
    {
        if (isset($donorAttrs['id'])) {
            return Donor::findOne($donorAttrs['id']);
        } else if (isset($donorAttrs['email'])) {
            return Donor::findOne(['email' => $donorAttrs['email']]);
        }
        return null;
    }

    private function createDonate(array $attrs): ?Donate
    {
        Yii::debug($attrs);
        try {
            $transaction = Yii::$app->db->beginTransaction();
            //creating Donor if necessary
            if (!isset($attrs['donorId']) || empty($attrs['donorId'])) {
                if (isset($attrs['donor']) && is_array($attrs['donor'])) {
                    $donor = $this->createOrUpdateDonor($attrs['donor']);
                    if ($donor) {
                        $attrs['donorId'] = $donor->id;
                    } else {
                        throw new Exception('Donor not exists');
                    }
                } else {
                    throw new Exception('Invalid params for Donor');
                }
            }
            unset($attrs['donor']);
            if (!isset($attrs['categoryId']) || empty($attrs['categoryId'])) {
                $attrs['categoryId'] = DonateCategory::getDefaultCategoryId();
            }
            $item = new Donate($attrs);
            if (!$item->save()) {
                $this->dumpModel($item);
                throw new Exception('Failed to save Donate');
            }
            $transaction->commit();
            return $item;
        } catch (\Throwable $e) {
            $transaction->rollBack();
            $this->dumpException($e);
            return null;
        }
    }


    public function createPayment(Donate $donate, array $attrs = []): ?Payment
    {
        Yii::debug($donate->attributes);
        Yii::debug($attrs);
        try {
            $transaction = Yii::$app->db->beginTransaction();
            $payment = new Payment($attrs);
            $payment->donateId = $donate->id;
            if (!$payment->isAttributeChanged('type', false)) {
                if ($donate->monthly) {
                    $payment->type = Payment::TYPE_MONTHLY_FIRST;
                } else {
                    $payment->type = Payment::TYPE_SINGLE;
                }
            }
            if (!$payment->isAttributeChanged('status', false)) {
                $payment->status = Payment::STATUS_PENDING;
            }
            $payment->amount = $donate->amount;
            if (!$payment->save()) {
                $this->dumpModel($payment);
                throw new Exception('Failed to save Payment');
            }
            $transaction->commit();
            return $payment;
        } catch (\Throwable $e) {
            $transaction->rollBack();
            $this->dumpException($e);
            return null;
        }
    }

    public function createOrUpdatePaymentServiceOperation(Payment $payment, string $operationId, array $attrs = []): ?PaymentServiceOperation
    {
        // Yii::debug($payment->attributes);
        // Yii::debug($operationId);
        // Yii::debug($attrs);
        try {
            $transaction = Yii::$app->db->beginTransaction();
            if (isset($attrs['response'])) {
                if (is_array($attrs['response'])) {
                    $attrs['response'] = Json::encode($attrs['response']);
                } else {
                    $attrs['response'] = (string)$attrs['response'];
                }
            }
            if (isset($attrs['request'])) {
                if (is_array($attrs['request'])) {
                    $attrs['request'] = Json::encode($attrs['request']);
                } else {
                    $attrs['request'] = (string)$attrs['request'];
                }
            }
            if (empty($attrs['recurrentOperationId'])) {
                $attrs['recurrentOperationId'] = null;
            }
            if (empty($attrs['request'])) { //to save old value on update
                unset($attrs['request']);
            }
            $item = PaymentServiceOperation::findOne(['paymentId' => $payment->id, 'donateId' => $payment->donateId]);
            if (!$item) {
                $item = new PaymentServiceOperation($attrs);
            } else {
                $item->setAttributes($attrs);
            }
            $item->paymentId = $payment->id;
            $item->donateId = $payment->donateId;
            $item->operationId = $operationId;
            if (!$item->save()) {
                $this->dumpModel($item);
                throw new Exception('Failed to save PaymentServiceOperation');
            }
            $transaction->commit();
            return $item;
        } catch (\Throwable $e) {
            $transaction->rollBack();
            $this->dumpException($e);
            return null;
        }
    }

    public function createOrUpdatePaymentServiceOperationByResponse(Response $response)
    {
        $meta = Metadata::createFromResponse($response);
        $payment = Payment::findOne($meta->paymentId);
        return $this->createOrUpdatePaymentServiceOperation(
            $payment,
            $response->paymentId,
            [
                'response' => $response->apiResponse,
                'request' => $response->apiRequest
            ]
        );
    }

    /**
     * Sends basic notification to Donor via INotificationService
     *
     * @param Payment $payment
     * @return boolean
     */
    public function notifyUserAfterPayment(Payment $payment): bool
    {
        if (!$this->notificationService) {
            Yii::warning("Notification service not found");
            return false;
        }
        $sended = null;
        if ($payment->isSingle) {
            if ($payment->isSucceeded) {
                $sended = $this->notificationService->notifyAfterSuccededSinglePayment($payment);
            } else if ($payment->isCanceled) {
                $sended = $this->notificationService->notifyAfterFailedSinglePayment($payment);
            }
        } else if ($payment->isMonthlyFirst) {
            if ($payment->isSucceeded) {
                $sended = $this->notificationService->notifyAfterSuccededFirstReccuringPayment($payment);
            } else if ($payment->isCanceled) {
                $sended = $this->notificationService->notifyAfterFailedFirstReccuringPayment($payment);
            }
        } else if ($payment->isMonthlyNext) {
            if ($payment->isSucceeded) {
                $sended = $this->notificationService->notifyAfterSuccededNextReccuringPayment($payment);
            } else if ($payment->isCanceled) {
                $sended = $this->notificationService->notifyAfterFailedNextReccuringPayment($payment);
            }
        }
        if ($sended === null) {
            Yii::warning("Notification did not send");
            return false;
        } else if ($sended === false) {
            Yii::error('Failed to notify');
            return false;
        }

        if (!$payment->notificationSent) {
            $payment->notificationSent = true;
            if (!$payment->save()) {
                Yii::error('Failed to save Payment');
                $this->dumpModel($payment);
            }
        }
        return true;
    }


    /**
     * Queries payment's status from payment service
     *
     * @param int|string|Payment $payment Payment object or one's ID
     * @return Response|null
     */
    public function queryPaymentInfo($payment): ?Response
    {
        try {
            if (is_numeric($payment)) {
                $payment = Payment::findOne($payment);
                if (!$payment) throw new Exception('Payment not found');
            } else if (!($payment instanceof Payment)) {
                throw new InvalidArgumentException('not a Payment model');
            }
            $this->clearLastPaymentServiceResponse();
            $response = $this->paymentService->queryPaymentStatus($payment->paymentServiceOperation->operationId);
            $this->setLastPaymentServiceResponse($response);
            return $response;
        } catch (\Throwable $e) {
            $this->dumpException($e);
            return null;
        }
    }



    private function dumpModel($model)
    {
        //attributes
        $message = get_class($model) . " model's attributes:\n";
        foreach ($model->attributes as $name => $value) {
            if (is_scalar($value)) {
                $message .= "$name = $value\n";
            } else {
                $message .= "$name = " . VarDumper::dumpAsString($value) . "\n";
            }
        }
        Yii::info($message);

        //errors
        $message = get_class($model) . " model's errors:\n";
        foreach ($model->errors as $name => $errors) {
            foreach ($errors as $error) {
                $message .= "[$name] $error\n";
            }
        }
        Yii::info($message);
    }


    private function dumpException(\Throwable $exception)
    {
        Yii::error($exception->getMessage() . "\nTrace:\n" . $exception->getTraceAsString());
    }
}
