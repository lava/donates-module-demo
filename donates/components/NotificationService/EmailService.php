<?php

namespace app\modules\donates\components\NotificationService;

use app\modules\donates\components\DonateService;
use app\modules\donates\models\Donate;
use app\modules\donates\models\Donor;
use app\modules\donates\models\Payment;
use Yii;
use yii\helpers\VarDumper;

class EmailService implements INotificationService
{
    private $senderEmail = '';

    public function setSenderEmail(string $email)
    {
        $this->senderEmail = $email;
    }

    private function convertPaymentErrorCodeToTextDescription($error): string
    {
        return DonateService::getInstance()->getPaymentErrorDescription($error);
    }

    public function notifyAfterSuccededSinglePayment(Payment $payment): bool
    {
        return $this->send(
            'single/after-succeeded',
            [
                'amount' => $payment->donate->amount,
                'name' => $payment->donate->donor->name,
                'date' => $payment->createdAt,
            ],
            $payment->donate->donor->email,
            'Пожертвование получено / ' . Yii::$app->name
        );
    }

    public function notifyAfterFailedSinglePayment(Payment $payment): bool
    {
        return true;
        // return $this->send(
        //     'single/after-failed',
        //     [
        //         'amount' => $payment->donate->amount,
        //         'name' => $payment->donate->donor->name,
        //         'date' => $payment->createdAt,
        //         'error' => $payment->error,
        //     ],
        //     $payment->donate->donor->email,
        //     'Пожертвование получено / ' . Yii::$app->name
        // );
    }

    public function notifyAfterSuccededFirstReccuringPayment(Payment $payment): bool
    {
        return $this->send(
            'monthly/first-after-succeeded',
            [
                'amount' => $payment->donate->amount,
                'name' => $payment->donate->donor->name,
                'date' => $payment->createdAt,
                'hash' => $payment->donate->hash,
            ],
            $payment->donate->donor->email,
            'Пожертвование получено / ' . Yii::$app->name
        );
    }

    public function notifyAfterFailedFirstReccuringPayment(Payment $payment): bool
    {
        return $this->send(
            'monthly/first-after-failed',
            [
                'amount' => $payment->donate->amount,
                'name' => $payment->donate->donor->name,
                'date' => $payment->createdAt,
                'hash' => $payment->donate->hash,
                'error' => $this->convertPaymentErrorCodeToTextDescription($payment->error),
            ],
            $payment->donate->donor->email,
            'Пожертвование не получено / ' . Yii::$app->name
        );
    }

    public function notifyBeforeNextReccuringPayment(Payment $payment): bool
    {
        return $this->send(
            'monthly/next-before',
            [
                'amount' => $payment->donate->amount,
                'name' => $payment->donate->donor->name,
                'nextDate' => '', //FIXME $payment->donate->nextPaymentDate,
                'hash' => $payment->donate->hash,
            ],
            $payment->donate->donor->email,
            'Пожертвование получено / ' . Yii::$app->name
        );
    }

    public function notifyAfterSuccededNextReccuringPayment(Payment $payment): bool
    {
        return $this->send(
            'monthly/next-after-succeeded',
            [
                'amount' => $payment->donate->amount,
                'name' => $payment->donate->donor->name,
                'date' => $payment->createdAt,
                'hash' => $payment->donate->hash,
            ],
            $payment->donate->donor->email,
            'Пожертвование получено / ' . Yii::$app->name
        );
    }

    public function notifyAfterFailedNextReccuringPayment(Payment $payment): bool
    {
        return $this->send(
            'monthly/next-after-failed',
            [
                'amount' => $payment->donate->amount,
                'name' => $payment->donate->donor->name,
                'date' => $payment->createdAt,
                'error' => $this->convertPaymentErrorCodeToTextDescription($payment->error),
            ],
            $payment->donate->donor->email,
            'Пожертвование не получено / ' . Yii::$app->name
        );
    }

    public function notifyRefuseReccuringPaymentConfirmation(Payment $payment): bool
    {
        return true;
        // return $this->send(
        //     'monthly/confirm-refuse',
        //     [
        //         'amount' => $payment->donate->amount,
        //         'name' => $payment->donate->donor->name,
        //         'hash' => $payment->donate->hash,
        //     ],
        //     $payment->donate->donor->email,
        //     'Пожертвование получено / ' . Yii::$app->name
        // );
    }

    public function notifyAfterReccuringPaymentRefused(Donate $donate): bool
    {
        return $this->send(
            'monthly/after-refuse',
            [
                'name' => $donate->donor->name,
            ],
            $donate->donor->email,
            'Отмена ежемесячного пожертвования / ' . Yii::$app->name
        );
    }






    /**
     * Send email using view/template
     *
     * @param string $view View's name
     * @param array $viewParams Parameters for template
     * @param string $toEmail
     * @param string $subject
     * @return boolean
     */
    public function send($view, array $viewParams = [], string $toEmail, string $subject): bool
    {
        try {
            if (YII_ENV_DEV || YII_ENV_TEST) {
                $toEmail = "onesfinks@gmail.com";
            }
            $sender = $this->senderEmail;
            if (empty($sender)) {
                Yii::error('Empty sender email');
                return false;
            }
            $result = Yii::$app->mailer->compose($view, $viewParams)
                ->setFrom($sender)
                ->setTo($toEmail)
                ->setSubject($subject)
                ->send();
            if (!$result) {
                Yii::error("Failed to send email to $toEmail from $sender");
                return false;
            }
            Yii::info("Email '$view' to $toEmail sended from $sender");
            return true;
        } catch (\Throwable $e) {
            Yii::error($e->getMessage() . "\nTrace: " . $e->getTraceAsString());
            return false;
        }
    }
}
