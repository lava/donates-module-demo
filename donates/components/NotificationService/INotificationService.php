<?php

namespace app\modules\donates\components\NotificationService;

use app\modules\donates\models\Donate;
use app\modules\donates\models\Payment;

interface INotificationService
{
    function setSenderEmail(string $email);

    /**
     * Уведомление об успешном одиночном донате
     *
     * @param Payment $payment
     * @return boolean
     */
    function notifyAfterSuccededSinglePayment(Payment $payment): bool;

    /**
     * Уведомление о неуспешном одиночном донате
     *
     * @param Payment $payment
     * @return boolean
     */
    function notifyAfterFailedSinglePayment(Payment $payment): bool;

    /**
     * Уведомление об успешном первом ежемесячном донате
     *
     * @param Payment $payment
     * @return boolean
     */
    function notifyAfterSuccededFirstReccuringPayment(Payment $payment): bool;

    /**
     * Уведомление о неуспешном первом ежемесячном донате
     *
     * @param Payment $payment
     * @return boolean
     */
    function notifyAfterFailedFirstReccuringPayment(Payment $payment): bool;

    /**
     * Предупреждение о предстоящем платеже ежемесячного доната
     *
     * @param Payment $payment
     * @return boolean
     */
    function notifyBeforeNextReccuringPayment(Payment $payment): bool;

    /**
     * Уведомление об успешном платеже ежемесячного доната
     *
     * @param Payment $payment
     * @return boolean
     */
    function notifyAfterSuccededNextReccuringPayment(Payment $payment): bool;

    /**
     * Уведомление о неуспешном платеже ежемесячного доната
     *
     * @param Payment $payment
     * @return boolean
     */
    function notifyAfterFailedNextReccuringPayment(Payment $payment): bool;

    /**
     * Сообщение с подтверждением отмены подписки ежемесячного доната
     *
     * @param Payment $payment
     * @return boolean
     */
    function notifyRefuseReccuringPaymentConfirmation(Payment $payment): bool;

    /**
     * Уведомление об отмене подписки ежемесячного доната
     *
     * @param Payment $payment
     * @return boolean
     */
    function notifyAfterReccuringPaymentRefused(Donate $donate): bool;
}
